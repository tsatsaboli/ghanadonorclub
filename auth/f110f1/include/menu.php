<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu">
      <li class="treeview">
        <a href="<?php echo WEB_ROOT;?>f110f1/view/?view=dashboard">
          <i class="fa fa-home"></i> <span>Dashboard</span> </a>
        </li>
        <li class="treeview">
          <a href="<?php echo WEB_ROOT;?>f110f1/view/?view=manage_users">
            <i class="fa fa-users"></i> <span>Manage Users</span> </a>
          </li>
          <li class="treeview">
            <a href="#">
              <i class="fa fa-level-up"></i>
              <span>Testimonials <?php if(count_testimonials('pending')){ ?><span class="badge"><?php echo count_testimonials("pending"); ?></span> <?php } ?></span>
              <span class="pull-right-container">
                <i class="fa fa-angle-right pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li><a href="<?php echo WEB_ROOT; ?>f110f1/view/?view=pending_testimonials"><i class="icon icon-arrow-right"></i>Pending <?php if(count_testimonials('pending')){ ?><span class="badge"><?php echo count_testimonials("pending"); ?></span> <?php } ?></a></li>
              <li><a href="<?php echo WEB_ROOT; ?>f110f1/view/?view=approved_testimonials"><i class="icon icon-arrow-right"></i>All approved <span class="badge"> <?php echo count_testimonials("approved"); ?></span></a></li>
              <!-- <li><a href="<?php echo WEB_ROOT; ?>view/?v=allrequest"><i class="icon icon-arrow-right"></i>All Upgraded Request</a></li> -->
            </ul>
          </li>
          <li class="treeview">
            <a href="<?php echo $self; ?>?logout">
              <i class="fa fa-sign-out"></i> <span>Log Out</span>
            </a>
          </li>
        </ul>
      </section>
      <!-- /.sidebar -->
    </aside>
