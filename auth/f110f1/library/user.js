function deleteUser(userId)
{
	if (confirm('Delete this user?')) {
		window.location.href = 'processUser.php?action=delete&userId=' + userId;
	}
}

function changeUserStatus(userId, status)
{
	var st = status == 'FALSE' ? 'Activate' : 'Deactivate'
	if (confirm('Do you want to ' + st+' this user?')) {
		window.location.href = 'processUser.php?action=status&userId=' + userId + '&nst=' + st;
	}
}
