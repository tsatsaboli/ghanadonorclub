<?php
if (!defined('WEB_ROOT')) {
  exit;
}
            
            $result = mysql_query("SELECT count(*) FROM tbl_users u, tbl_accounts a WHERE u.id = a.user_id AND level='Free user'");
            $row = dbFetchArray($result);
            $count = $row['0'];

            $result1 = mysql_query("SELECT count(*) FROM tbl_users u, tbl_accounts a WHERE u.id = a.user_id AND level='Level 1'");
            $row1 = dbFetchArray($result1);
            $count1 = $row1['0'];

            $result2 = mysql_query("SELECT count(*) FROM tbl_users u, tbl_accounts a WHERE u.id = a.user_id AND level='Level 2'");
            $row2 = dbFetchArray($result2);
            $count2 = $row2['0'];

            $result3 = mysql_query("SELECT count(*) FROM tbl_users u, tbl_accounts a WHERE u.id = a.user_id AND level='Level 3'");
            $row3 = dbFetchArray($result3);
            $count3 = $row3['0'];

            $sql = "SELECT * FROM tbl_users u, tbl_accounts a, tbl_address ad WHERE u.id = a.user_id AND ad.user_id = u.id ORDER BY fullname ASC";
    
            $result = mysql_query($sql);
?>
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3><?php echo $count?></h3>

              <p>Free Users Total</p>
            </div>
         
            
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3><?php echo $count1?></h3>

              <p>Level 1 Total</p>
            </div>
         
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3><?php echo $count2?></h3>

              <p>Level 2 Total</p>
            </div>
            
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3><?php echo $count3?></h3>

              <p>Level 3 Total</p>
            </div>
      
          </div>
        </div>
        <!-- ./col -->
      </div>
      <!-- /.row -->
      <!-- Main row -->
     <div class="row">
        <div class="col-xs-12">

          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Registered Members</h3>
            </div>
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped table-responsive no-padding">
                <thead>
                <tr>
                  <th>Full Name</th>
                  <th>Username</th>
                  <th>Gender</th>
                  <th>Level</th>
                  <th>Account Status</th>
                </tr>
                </thead>
                <tbody>
                <?php 
                while($row = dbFetchAssoc($result)) {
                extract($row);
?>
                <tr>
                  <td><?php echo $fullname; ?></td>
                  <td><?php echo $pin; ?></td>
                  <td><?php echo $gender; ?></td>
                  <td><?php echo $level; ?></td>
                  <td><?php echo $is_active == 'FALSE'? 'Inactive' : 'Active'; ?></td>
                </tr>
                <?php }?>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->