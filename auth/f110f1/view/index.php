<?php
require_once '../../library/config.php';
require_once '../library/functions.php';
$self = WEB_ROOT . 'f110f1/index.php';

$_SESSION['login_return_url'] = $_SERVER['REQUEST_URI'];
checkAdmin();

$view = (isset($_GET['view']) && $_GET['view'] != '') ? $_GET['view'] : '';

switch ($view) {

	case 'view_users' :
		$content    = 'view_users.php';
		$pageTitle  = 'View Users';
	break;

	case 'manage_users' :
		$content    = 'manage_users.php';
		$pageTitle  = 'Manage Users';
	break;

	case 'user_logs' :
		$content    = 'user_logs.php';
		$pageTitle  = 'Manage Users';
	break;

	case 'activity_logs' :
		$content    = 'activity_logs.php';
		$pageTitle  = 'Manage Users';
	break;

	case 'pending_testimonials':
		$content = 'pending_testimonials.php';
		$pageTitle = 'Pending Testimonials';
		break;

	case 'approved_testimonials':
		$content = 'approved_testimonials.php';
		$pageTitle = 'Approved Testimonials';
		break;

	case 'approve_testimonial':
		approve_testimonial($_REQUEST['id']);
		break;

		case 'decline_testimonial':
		decline_testimonial($_REQUEST['id']);
		break;

	default :
		$content 	= 'dashboard.php';
		$pageTitle 	= 'Dashboard';
	break;
}

$script    = array('user.js','jquery.min.js');
require_once '../include/template.php';
?>
