<?php
if (!defined('WEB_ROOT')) {
  exit;
}

$sql = "SELECT * FROM tbl_users u, tbl_accounts a, tbl_address ad WHERE u.id = a.user_id AND ad.user_id = u.id ORDER BY fullname ASC";

$result = mysql_query($sql) or die();
?>
    <section class="content">
     <div class="row">
        <div class="col-xs-12">

          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Manage User Lists</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
            <form action="processUser.php?action=delete" method="post"  name="frmListUser" id="frmListUser">


              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Full Name</th>
                  <th>Username</th>
                  <th>Gender</th>
                  <th>Level</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <?php
                while($row = dbFetchAssoc($result)) {
                 extract($row);
?>
                <tr>
                  <td><?php echo $fullname; ?></td>
                  <td><?php echo $pin; ?></td>
                  <td><?php echo $gender; ?></td>
                   <td <?php if($level == "Free user" && $status == 'False') {echo 'style="background-color: red; color:white"';}  ?>><?php echo $level; ?></td>
                  <td><a href="<?php echo WEB_ROOT;?>f110f1/view/?view=view_users&userId=<?php echo $id; ?>">View Details</a></td>
                </tr>
                <?php }?>
                </tbody>
              </table>

            </form>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
