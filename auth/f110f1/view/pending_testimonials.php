<div class="col-md-12">
<div class="box-header with-border">
          <h3 class="box-title">Testimonials</h3>
        </div>
          <!-- Custom Tabs -->
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#tab_2" data-toggle="tab">Pending Testimonials</a></li>
            </ul>
            <div class="tab-content">

              <!-- /.tab-pane -->
              <div class="tab-pane active" id="tab_2">
                <div class="row">
                <div class="box-body">
                <p>Kindly approve the following testimonials from users </p>
                <p><?php echo $errorMessage?></p>
                <div class="table-responsive">
                <div class="box-body">
              <!-- /.table-responsive -->
            </div>
            <table class="table">

              <tbody>
                <?php $requests = getTestimonials();$i=1;
                // $request here is pending_testimonials
                 ?>
                <?php foreach($requests as $request) {?>
                  <?php if(!$request['approval'] ){ ?>
                    <tr>
                      <td>Full Name</td>
                      <td><?php echo $request['fullname']; ?></td>
                    </tr>
                    <tr>
                      <td>User(s) Level</td>
                      <td><?php echo $request['level']; ?></td>
                    </tr>
                    <tr>
                      <td>Testimonial</td>
                      <td><?php echo $request['message']; ?></td>
                    </tr>
                    <tr >
                      <td>Email</td>
                      <td><?php echo $request['email'] ?></td>
                    </tr>
                    <tr>
                      <td>Status</td>
                      <td><?php  echo (!$request['approval'])?  'Pending':'Approved'; ?></td>
                    </tr>
                    <tr><td colspan="2">

                    <button  class="btn btn-sm btn-danger btn-flat" onclick="declineTestimonial(<?php echo $request['t_id'];?>)"><i>Decline</i></button> 

                    <button  class="btn btn-sm btn-info btn-flat" onclick="approveTestimonial(<?php echo $request['t_id'];?>)"><i>Approve</i></button>

                    </td> </tr>

                      <?php  }} ?>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>


          <!-- /.tab-pane -->
        </div>
        <!-- /.tab-content -->
      </div>
      <!-- nav-tabs-custom -->
    </div>

        <script type="text/javascript">

        function declineTestimonial(id){


            var declineConfirmation = confirm(`Click ok to decline approval`);

            if(declineConfirmation){
              
              var data ={
                id: id,
              };

              $.post('?view=decline_testimonial',data).then((response)=>{
                if(response)
                window.location = window.location;
                console.log(reponse);
              },(err)=>{
                console.log(err);
              })
            }
          }

          function approveTestimonial(id){


            var confirmation = confirm(`Click ok to confirm approval`);

            if(confirmation){

              //issue upgrade http request

              var data ={
                id: id,
              };

              $.post('?view=approve_testimonial',data).then((response)=>{
                if(response)
                window.location = window.location;
                console.log(reponse);
              },(err)=>{
                console.log(err);
              })
            }
          }
        </script>
