<?php
require_once '../../library/config.php';
require_once '../library/functions.php';

checkAdmin();

$action = isset($_GET['action']) ? $_GET['action'] : '';

switch ($action) {
			
	case 'delete' :
		deleteUser();
		break; 

		case 'status' :
		statusUser();
		break;   

	default :
	    // if action is not defined or unknown
		// move to main user page
		header('Location: index.php?view=manage_users');
	break;
}

function statusUser()
{
	$userId = (int)$_GET['userId'];	
	$nst 	= $_GET['nst'];
	
	$status = $nst == 'Activate' ? 'TRUE' : 'FALSE';
	$sql   = "UPDATE tbl_users 
	          SET is_active = '$status'
			  WHERE id = $userId";

	dbQuery($sql);
	header('Location: index.php?view=dashboard');	

}
/*
	Remove a user
*/
function deleteUser()
{
	if (isset($_GET['userId']) && (int)$_GET['userId'] > 0) {
		$userId = (int)$_GET['userId'];
	} else {
		header('Location: index.php');
	}
		
	$sql = "DELETE FROM tbl_users WHERE id = $userId";
	dbQuery($sql);
	
	$sql = "DELETE FROM tbl_accounts WHERE user_id = $userId";
	dbQuery($sql);
	
	$sql = "DELETE FROM tbl_address WHERE user_id = $userId";
	dbQuery($sql);
	
	header('Location: index.php?view=dashboard');
}
?>