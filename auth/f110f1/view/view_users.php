<?php
if (!defined('WEB_ROOT')) {
  exit;
}

if (isset($_GET['userId']) && (int)$_GET['userId'] > 0) {
  $userId = (int)$_GET['userId'];
} else {
  header('Location: index.php');
}

$errorMessage = (isset($_GET['error']) && $_GET['error'] != '') ? $_GET['error'] : '&nbsp;';

$sql = "SELECT * FROM tbl_users u, tbl_accounts a, tbl_address ad
    WHERE u.id = a.user_id AND ad.user_id = u.id
    AND a.id = $userId";

//$sql = "SELECT * FROM tbl_users WHERE id = $userId";
$result = dbQuery($sql);
extract(dbFetchAssoc($result));


?>
<br>    <!-- Main content -->
    <section class="invoice">
      <div class="row">
        <div class="col-xs-12">
                   <a href="javascript:changeUserStatus(<?php echo $id; ?>, '<?php echo $is_active; ?>');" class="btn btn-primary pull-right" style="margin-right: 5px;"><?php echo $is_active == 'FALSE'? 'Activate User' : 'Deactivate User'; ?>
                   <a href="javascript:deleteUser(<?php echo $id?>);" class="btn btn-primary pull-right" style="margin-right: 5px;">Delete User</a>
          </button>
          <h2 class="page-header">Registered Members Information
            <small class="pull-right"></small>
          </h2>
        </div>
        <!-- /.col -->
      </div>

      <!-- info row -->
      <div class="row invoice-info">
        <!-- /.col -->
        <div class="col-md-12 invoice-col">
          <b>Ghana Donor Club</b>
          <br>
          <b>Todays Date:</b> <?php echo date('d/m/Y')?><br>
          <p style="text-align: center;">This page contains all the information of a registered member in the Ghana Donor Club</p>
        </div>
      <div class="row">
        <div class="col-md-12">
          <div class="table-responsive">
            <table class="table" width="100">
              <tr>
                <th style="width:35%">Fullname:</th>
                <td><?php echo $fullname; ?></td>
              </tr>
                <tr>
                <th>UserID:</th>
                <td><?php echo $username; ?></td>
              </tr>
              <tr>
                <th>Email:</th>
                <td><?php echo $email; ?></td>
              </tr>
              <tr>
                <th>Phone:</th>
                <td><?php echo $phone; ?></td>
              </tr>
              <tr>
                <th>Gender:</th>
                <td><?php echo $gender; ?></td>
              </tr>
              <tr>
                <th>Account Status:</th>
                <td><?php echo $is_active == 'FALSE'? 'Inactive' : 'Active'; ?></td>
              </tr>
              <tr>
                <th>Registered Date & Time:</th>
                <td><?php echo $inputteddate; ?></td>
              </tr>
              <tr>
                <th>City:</th>
                <td><?php echo $city; ?></td>
              </tr>
               <tr>
                <th>Country:</th>
                <td><?php echo $country; ?></td>
              </tr>
                <tr>
                <th>Level:</th>
                <td><?php echo $level; ?></td>
              </tr>
                <tr>
                <th>Payment Type:</th>
                <td><?php echo $type; ?></td>
              </tr>
                <tr>
                <th>Account Type:</th>
                <td><?php echo $acct_type; ?></td>
              </tr>
                <tr>
                <th>Account Name:</th>
                <td><?php echo $acct_name; ?></td>
              </tr>
                <tr>
                <th>Acoount Number:</th>
                <td><?php echo $acct_number; ?></td>
              </tr>
              <tr>
                <th>Username:</th>
                <td><?php echo $pin; ?></td>
              </tr>
              <tr>
                <th>Registeration Status:</th>
                <td><?php echo $status; ?></td>
              </tr>
            </table>
          </div>
        </div>
        <!-- /.col -->
      </div>

      </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->
