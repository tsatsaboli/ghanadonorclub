<ul class="sidebar-menu">
        <li class="header">MAIN NAVIGATION</li>
         <li><a href="<?php echo WEB_ROOT;?>view/?v=dashboard"><i class="fa fa-home"></i> <span>Dashboard</span></a> </li>
    <li> <a href="<?php echo WEB_ROOT;?>view/?v=profile"><i class="fa fa-user"></i> <span>My Profile (Edit)</span></a>
    </li>
     <li><a href="<?php echo WEB_ROOT;?>view/?v=testimonials"><i class="fa fa-envelope"></i> <span>Testimonials</span></a> </li>
            <li class="treeview">
          <a href="#">
            <i class="fa fa-level-up"></i>
            <span>My Upgrade Options</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo WEB_ROOT; ?>view/?v=levelremain"><i class="icon icon-arrow-right"></i>Level(s) Remaining</a></li>
        <li><a href="<?php echo WEB_ROOT; ?>view/?v=upgraderequest"><i class="icon icon-arrow-right"></i>Upgrade Request</a></li>
        <li><a href="<?php echo WEB_ROOT; ?>view/?v=allrequest"><i class="icon icon-arrow-right"></i>All Upgraded Request</a></li>
      </ul>
        </li>
                <li class="treeview">
          <a href="#">
            <i class="fa fa-level-down"></i>
            <span>My Downline Options</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo WEB_ROOT;?>view/?v=downlines"><i class="icon icon-arrow-right"></i>My Downlines Tree</a></li>
        <li><a href="<?php echo WEB_ROOT;?>view/?v=downlinesinfo"><i class="icon icon-arrow-right"></i>Pending Downline Info</a></li>
        <li><a href="<?php echo WEB_ROOT;?>view/?v=find_downlines"><i class="icon icon-arrow-right"></i>Find A Downline</a></li>
      </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-money"></i>
            <span>My Payments/Details</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo WEB_ROOT;?>view/?v=upliner_details"><i class="icon icon-arrow-right"></i>My Upliners Details</a></li>
        <li><a href="<?php echo WEB_ROOT;?>view/?v=earnings"><i class="icon icon-arrow-right"></i>My Earnings</a></li>
      </ul>
        </li>
        <li><a href="<?php echo WEB_ROOT;?>view/?v=customer_service"><i class="fa fa-users"></i> <span>Customer Service</span></a> </li>
    <li><a href="<?php echo WEB_ROOT; ?>?logout"><i class="fa fa-sign-out"></i> <span>Logout</span></a>
      </ul>
