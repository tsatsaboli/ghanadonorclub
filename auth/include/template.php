<?php
  $id = $_SESSION['hlbank_user']['id'];
  $sql = "SELECT * FROM tbl_users u, tbl_accounts a, tbl_address ad
  WHERE u.id = a.user_id AND ad.user_id = u.id AND u.id ='$id'";
  $result = dbQuery($sql);
  //$result = dbQuery($sql);
  $row = dbFetchAssoc($result);
  $usrLevel = $row['level'];
  $username = $row['fullname'];
  $usr = $row['pin'];
  $usrID = $row['username'];
  $ref=$row['referral_code'];

  if ($usrLevel !='Free user') {
    $refLink="<a href='https://www.ghanadonor.club/auth/register.php?ref=".$ref."' target='_blank'>https://www.ghanadonor.club/auth/register.php?ref=$ref</a>";
  }else{
    $refLink="Sorry! Your referral link will only be active after you upgrade to level 1";
  }

  $result = dbQuery("SELECT * FROM tbl_users u, testimonials t
  WHERE u.id = t.user_id AND t.approval='1'");
  while ($row = dbFetchAssoc($result)) {
    $name = $row["fullname"];
    $msg = $row['message'];
  }
?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo $pageTitle?></title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?php echo WEB_ROOT;?>bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo WEB_ROOT;?>font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="<?php echo WEB_ROOT;?>plugins/datatables/dataTables.bootstrap.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo WEB_ROOT;?>dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo WEB_ROOT;?>dist/css/skins/_all-skins.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <style type="text/css">

  .greeting {
  font-weight: 600;

  font-size: 30px
}
</style>

</head>
<body class="hold-transition skin-blue sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">
 <?php include('top_nav.php');?>

  <!-- =============================================== -->

  <!-- Left side column. contains the sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?php echo WEB_ROOT;?>dist/img/user.png" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><?php echo $username;?></p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <?php include('menu.php');?>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- =============================================== -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Main content -->
    <section class="content">

      <!-- notify user to upgrade -->
      <?php
        $next_level = notify_user_upgrade_to_next_level($_SESSION['hlbank_user']['id']);
        if(isset($next_level)){
          $compel_upgrade_time = disable_on_compel_fail($_SESSION['hlbank_user']['id'],$next_level);
          if(isset($compel_upgrade_time)){
       ?>
       <div class="alert alert-dismissable alert-danger">
         <strong>Please upgrade to </strong> <?php echo $next_level; ?> or your account will be deleted in <time></time>
       </div>

       <?php }else{ ?>
         <div class="alert alert-dismissable alert-danger">
           <strong>Your account has been deactivated </strong> Please request for an upgrade immediately
         </div>
         <?php }} ?>
      <!-- Default box -->
      <div class="box">

        <div class="box-body">
        <marquee><i class="fa fa-envelope-o fa-fw"></i>Members Testimonials
        <?php
          $result = dbQuery("SELECT * FROM tbl_users u, testimonials t WHERE u.id = t.user_id AND t.approval='1'");
                    while ($row = dbFetchAssoc($result)) {
        ?>
        <?php echo "<strong style='color: orange'>Name: </strong>". $row["fullname"] ." "."<strong style='color: green'>Says: </strong>".$row['message']?>
        <?php
  }
        ?>
        </marquee>
           <?php

              require_once $content;
              ?>
        </div>
        <!-- /.box-body -->

        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 1.0
    </div>
    <strong>Copyright &copy;2017 All rights
    reserved.
  </footer>
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 2.2.3 -->
<script src="<?php echo WEB_ROOT;?>plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="<?php echo WEB_ROOT;?>bootstrap/js/bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="<?php echo WEB_ROOT;?>plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?php echo WEB_ROOT;?>plugins/fastclick/fastclick.js"></script>
<!-- DataTables -->
<script src="<?php echo WEB_ROOT;?>plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo WEB_ROOT;?>plugins/datatables/dataTables.bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="<?php echo WEB_ROOT;?>plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?php echo WEB_ROOT;?>plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo WEB_ROOT;?>dist/js/app.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo WEB_ROOT;?>dist/js/demo.js"></script>
<!-- page script -->

<script>
  $(function () {
    $("#example1").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
  });
</script>

<script type="text/javascript">
  var thehours = new Date().getHours();
  var themessage;
  var morning = ('Good morning');
  var afternoon = ('Good afternoon');
  var evening = ('Good evening');

  if (thehours >= 0 && thehours < 12) {
    themessage = morning;

  } else if (thehours >= 12 && thehours < 17) {
    themessage = afternoon;

  } else if (thehours >= 17 && thehours < 24) {
    themessage = evening;
  }

  $('.greeting').append(themessage);

</script>

  <?php if(isset($compel_upgrade_time)){ ?>

    <script>

    var compel_upgrade_minutes_remaining =  <?php echo $compel_upgrade_time | 'undefined'?>;

    function updateTime(){
      if(compel_upgrade_minutes_remaining == 'undefined')
        return
      compel_upgrade_minutes_remaining --;
      var hours = parseInt(compel_upgrade_minutes_remaining / 60);
      var minutes = parseInt(compel_upgrade_minutes_remaining % 60);
      var time = `${hours} Hours ${minutes} Minutes`;

      $('time').html(time);

      setTimeout(updateTime,1000*60);

      // console.log(time,' time remaining');
    }

    updateTime();

    // var timerUpdate = setInterval(()=>{
    //   updateTime();
    // },1000 * 60);

    </script>

    <?php }
      if(showTestimonialInputPopUp($_SESSION['hlbank_user']['id'])){
        echo $_SESSION['hlbank_user']['id'];
     ?>

     <script type="text/javascript">
      $('body').html(`
        <div class="col-md-12">
        <div class="box-header with-border">
                  <h3 class="box-title">Testimonials</h3>
                </div>
                  <!-- Custom Tabs -->
                  <div class="nav-tabs-custom">

                    <?php if(isset($_GET['flash'])) {?>
                      <div class="alert alert-dismissable alert-success">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <?php if($_GET['flash']){ ?>
                          <h4>Testimonial added successfully!</h4>
                          <?php }else{ ?>
                            <h4>Thank you, but it seems you have submitted a testimonial already!</h4>
                            <?php } ?>
                        <!-- <p>Best check yo self, you're not looking too good. Nulla vitae elit libero, a pharetra augue. Praesent commodo cursus magna, <a href="#" class="alert-link">vel scelerisque nisl consectetur et</a>.</p> -->
                      </div>
                    <?php } ?>
                    <ul class="nav nav-tabs">
                      <li class="active"><a href="#tab_3" data-toggle="tab">Write us a testimonial</a></li>
                    </ul>
                    <div class="tab-content">
                      <!-- /.tab-pane -->
                      <div class="tab-pane active" id="tab_3">
                        <div class="row">
                       <div class="box-body">
                       <p>Write your testimonial</p>
                         <form class="form-horizontal form-label-left" action="add_testimonial.php" method="post">
                         <div class="box-body">
                       <div class="form-group">
                          <label for="exampleInputEmail1">Share your Testimonial</label>
                          <textarea class="form-control" name="message" minlength='4' spellcheck='true' required></textarea>

                        </div>

                        <input type="text" class='hidden' name="user_id" value="<?php echo $_SESSION['hlbank_user']['id']; ?>">
                      </div>
                      <!-- /.box-body -->

                      <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Send your Testimonial</button>
                      </div>
                    </form>
                     </div>
              </div>
                      </div>
                      <!-- /.tab-pane -->
                    </div>
                    <!-- /.tab-content -->
                  </div>
                  <!-- nav-tabs-custom -->
                </div>

        `);
     </script>
     <?php } ?>
</body>
</html>
