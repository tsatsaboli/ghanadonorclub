<?php
//require_once('mail.php');




/*
	Check if a session user id exist or not. If not set redirect
	to login page. If the user session id exist and there's found
	$_GET['logout'] in the query string logout the user
*/
function checkUser()
{
	// if the session id is not set, redirect to login page
	if (!isset($_SESSION['hlbank_user'])) {
		header('Location: ' . WEB_ROOT . 'login.php');
		exit;
	}
	// the user want to logout
	if (isset($_GET['logout'])) {
		doLogout();
	}
}


function next_tx_no() {
	$sql = "SELECT tx_no FROM tbl_transaction ORDER BY id DESC LIMIT 1";
	$result = dbQuery($sql);
	extract(dbFetchAssoc($result));
	$tx_num		= (int)substr($tx_no, 2);
	$next_id 	= $tx_num+1; // increment by One
	return 'TX'.$next_id;
}

function str_number($str) {
	$number = '';
	$number = str_replace('$', '', $str);
	$number = str_replace(',', '', $number);
	return doubleval($number);
}

function doPinValidation() {
	$errorMessage = '';

	$pin = $_SESSION['hlbank_tmp']['pin'];
	$ipPin = $_POST['accpin'];

	if($pin == $ipPin) {
		$_SESSION['hlbank_user'] = $_SESSION['hlbank_tmp'];
		unset($_SESSION['hlbank_tmp']);
		header('Location: dashboard.php♦');
		exit;
	}
	else {
		$errorMessage = '<div class="alert alert-danger alert-dismissible fade in" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                    </button>
                    Invalid pin numbers, please try again.
                  </div>';
	}
	return $errorMessage;
}
/*

*/

function doLogin()
{
	$errorMessage = '';

	$accno 	= $_POST['accno'];
	$pwd 	= $_POST['pass'];


	$sql = "SELECT * FROM tbl_users u, tbl_accounts a, tbl_address ad
			WHERE a.pin = '$accno' AND u.pwd = PASSWORD('$pwd')
			AND u.id = a.user_id AND ad.user_id = u.id AND u.is_active != 'FALSE'";
	$result = dbQuery($sql);

	if (dbNumRows($result) == 1) {
		$row = dbFetchAssoc($result);
		$_SESSION['hlbank_tmp'] = $row;
		$_SESSION['hlbank_user_name'] =	strtoupper( $row['fullname']);
		$_SESSION['hlbank_user'] = $_SESSION['hlbank_tmp'];
		unset($_SESSION['hlbank_tmp']);
		header('Location: dashboard.php');
		exit;
	}
	else {
		$errorMessage = '<div class="alert alert-danger alert-dismissible fade in" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                    </button>
                    Invalid login, please try again.
                  </div>';
	}
	return $errorMessage;
}




function doResetPassword()
{

/**
 * This example shows settings to use when sending via Google's Gmail servers.
 */

//SMTP needs accurate times, and the PHP time zone MUST be set
//This should be done in your php.ini, but this is how to do it if you don't have access to that
date_default_timezone_set('Etc/UTC');

require '../PHPMailerAutoload.php';

//Create a new PHPMailer instance
$mail = new PHPMailer;

//Tell PHPMailer to use SMTP
$mail->isSMTP();

//Enable SMTP debugging
// 0 = off (for production use)
// 1 = client messages
// 2 = client and server messages
$mail->SMTPDebug = 2;

//Ask for HTML-friendly debug output
$mail->Debugoutput = 'html';

//Set the hostname of the mail server
$mail->Host = 'smtp.gmail.com';
// use
// $mail->Host = gethostbyname('smtp.gmail.com');
// if your network does not support SMTP over IPv6

//Set the SMTP port number - 587 for authenticated TLS, a.k.a. RFC4409 SMTP submission
$mail->Port = 587;

//Set the encryption system to use - ssl (deprecated) or tls
$mail->SMTPSecure = 'tls';

//Whether to use SMTP authentication
$mail->SMTPAuth = true;

//Username to use for SMTP authentication - use full email address for gmail
$mail->Username = "username@gmail.com";

//Password to use for SMTP authentication
$mail->Password = "yourpassword";

//Set who the message is to be sent from
$mail->setFrom('from@example.com', 'First Last');

//Set an alternative reply-to address
$mail->addReplyTo('replyto@example.com', 'First Last');

//Set who the message is to be sent to
$mail->addAddress('whoto@example.com', 'John Doe');

//Set the subject line
$mail->Subject = 'PHPMailer GMail SMTP test';

//Read an HTML message body from an external file, convert referenced images to embedded,
//convert HTML into a basic plain-text alternative body
$mail->msgHTML(file_get_contents('contents.html'), dirname(__FILE__));

//Replace the plain text body with one created manually
$mail->AltBody = 'This is a plain-text message body';

//Attach an image file
$mail->addAttachment('images/phpmailer_mini.png');

//send the message, check for errors
if (!$mail->send()) {
    echo "Mailer Error: " . $mail->ErrorInfo;
} else {
    echo "Message sent!";
}

}
/*
	Logout a user
*/

function doChagePassword()
{
	$errorMessage = '';

	   // Gather the post data
    $email = $_POST["resetEmail"];
    $password = $_POST["resetPass"];
    $confirmpassword = $_POST["resetConfirmPass"];
    $hash = $_POST["q"];

    // Use the same salt from the forgot_password.php file
    $salt = "498#2D83B631%3800EBD!801600D*7E3CC13";
    $resetkey = hash('sha512', $salt.$email);

	// Does the new reset key match the old one?
    if ($resetkey == $hash)
    {
        if ($password == $confirmpassword)
        {
            $password = hash('sha512', $salt.$password);

            $sql = "UPDATE users SET password=password WHERE email=email'";
	        $result = dbQuery($sql);
            //echo "Your password has been successfully reset.";
        }else{
            //echo "Your password's do not match.";
        }
    }
    else{
        //echo "Your password reset key is invalid.";
    }

}

function doLogout()
{
	if (isset($_SESSION['hlbank_user'])) {
		unset($_SESSION['hlbank_user']);
		//session_unregister('hlbank_user');
	}
	header('Location: ../');
	exit;
}


function doRegister()
{
	$fname 	= $_POST['fullname'];
	$email 	= $_POST['email'];
	$phone 	= $_POST['phone'];
	$country 	= $_POST['country'];
	$city = $_POST['city'];
	$gender = $_POST['gender'];
	$pwd 	= $_POST['password'];
	$pin 	= $_POST['pin'];
	$paymentmode = $_POST['paymentmode'];

	//$mMType	= $_POST['mMType'];
	$mMName 	= $_POST['mMName'];
	$mMNumber	= $_POST['mMNumber'];
	$accType	= $_POST['acctype'];
	$ipaddress = $_SERVER['REMOTE_ADDR'];

	//check if referral_by submitted is valid
		if(isset($_POST['referral_by']) && $_POST['referral_by'] != ''){
			if(!isReferral_byValid($_POST['referral_by'])){
				//go back to registration page if referral_by is invalid
				$address = 'register.php?';
				$i = 0;
				foreach($_POST as $key =>$val){
					if($key == 'password'){
						continue;
					}

					if($i++==0){
						$address = sprintf('%sref_err=true&%s=%s',$address,$key,urlencode($val));
					}else{

						$address = sprintf('%s&%s=%s',$address,$key,urlencode($val));
					}
				}

				header('Location: '.$address);
			}
		}

		// return;

	$original_referral_by = getAParent($_POST);
	$referral_by = getVAlidReferral($original_referral_by);

	$errorMessage = '';

	$sql = "SELECT pin FROM tbl_accounts WHERE pin = '$pin'";
	$result = dbQuery($sql);
	if (dbNumRows($result) == 1) {
		$errorMessage='
		  <div class="alert alert-danger" style="text-align: center">
             <p>Username is already exist, please try another name.</p>
         </div>
            ';
		return $errorMessage;
	}

		//first check if account number is already register or not...
	$accno = rand(9999999999, 99999999999);
	$accno = strlen($accno) != 10 ? substr($accno, 0, 10) : $accno;

	//first check if account number is already register or not...
    $character_set_array = array();
    $character_set_array[] = array('count' => 4, 'characters' => 'ABCDEFGHIJKLMNOPQRSTUVWXYZ');
    $character_set_array[] = array('count' => 4, 'characters' => '0123456789');
    $temp_array = array();
    foreach ($character_set_array as $character_set) {
        for ($i = 0; $i < $character_set['count']; $i++) {
            $temp_array[] = $character_set['characters'][rand(0, strlen($character_set['characters']) - 1)];
        }
    }
    shuffle($temp_array);
    //echo $temp_array;
    $ref= implode('', $temp_array);

	$insert_id = 0;
	$sql = "INSERT INTO tbl_users (fullname,username,pwd, email, phone, gender, is_active, utype,usrip, inputteddate,referral_code,referral_by,original_referral_by)
			VALUES ('$fname','$accno', PASSWORD('$pwd'), '$email', '$phone', '$gender', 'True', 'User','$ipaddress',NOW(),'$ref','$referral_by','$original_referral_by')";
	dbQuery($sql);
	$insert_id = dbInsertId();

	//now create a user address.
	$sql = "INSERT INTO tbl_address (user_id, city, country)
			VALUES ('$insert_id', '$city', '$country')";
	dbQuery($sql);

	//and now create a account table entry...
	$sql = "INSERT INTO tbl_accounts (user_id,level, type, acct_type, acct_name, acct_number, pin, status)
			VALUES ($insert_id, 'Free user', '$paymentmode', '$accType', '$mMName', '$mMNumber', '$pin','False')";
	dbQuery($sql);

	//$from = "Ghana Donor Club <no-reply@ghanadonor.club>";
    $subject = "Account Registration";
	$to = $email;
	$msg_body = "<strong>Congratulations</strong> !!! You are now a member of Ghana Donor Club<br><br>

Dear $fname; Thank You for your interest in this program. Below is your account information<br><br>

Username#: $pin<br>
Password#: $pwd (please change after login)<br>
Email: $email<br>
Login: <a href='https://www.ghanadonor.club/auth/login.php'>https://www.ghanadonor.club/auth/login.php</a><br>
Ref Link : <a href='https://www.ghanadonor.club/auth/register.php?ref=$ref'>https://www.ghanadonor.club/auth/register.php?ref=$ref</a><br><br>

<h6>GHC320 WITHIN A DAY</h6>
					<p>Yes!!! Make GHC320 within a day (Three hundred and twenty Ghana cedis) with only Ghc50 initial investment. Your dreams can come true!! </p><br>


<p>This is legit and there is no central account.
Initial Start up capital is GHC50(Low risk) .
Final profit is GHC320 WITHIN A DAY.</p><br>
					<p>It's a 2×2 Member to member matrix donations. Join now it's fresh and cycle out faster than any other Multi Level Marketing (MLM) Register for free but you must pay Ghc50 within 3hrs of registration to upliner for upgrade to level 1. Note: always call upliner before and after payment for confirmation.</p><br>
<p>Level 0: This a free user level. You become an active member at level 1. At this level you pay your upliner Ghc50 for upgrade to level 1.</p><br>


<p>Level 1: At this level you invite 2 people and you also receive Ghc50 each from your 2 downlines making Ghc100Ghc Then you'll remove Ghc80 to upgrade yourself to level 2 . At this point your profit in level 1 is Ghc20</p><br>

<p>Level 2: This is the final level. At this level you receive Ghc80 from 4 people on level 2downlines and that's Ghc320. </p><br>
<p>And then u can recircle again. With only Ghc50.</p>

<p>Give glory to God and recycle back to start another level for more millions.</p>
<br><br>
<strong>Enjoy giving and receiving donations with</strong><br>
<strong>GHANA DONOR.CLUB</strong><br>
<strong>TEAM, GHANA DONOR.CLUB</strong><br>

";


 $mail_data = array('to' => $to, 'sub' => $subject, 'msg' => 'register', 'body' => $msg_body);
	send_email($mail_data);

 echo "<script>
alert('Congratulations! You are now a member of Ghana Donor Club');
window.location.href='login.php';
</script>";
	exit;

}
/**/
function usersOnline($pgcod)
{
$ipaddress = $_SERVER['REMOTE_ADDR'];
userInsert($pgcod,$ipaddress);
usersDelete($pgcod,$ipaddress);
$pageuserscount=usersCount($pgcod);	//Total Users on Page
$totaluserscount=usersTotalCount();	//Total Users on Site
$maxpageuserscount=usersmaxCount($pgcod,$pageuserscount); //Maximum Users on Page
$maxtotaluserscount=usersmaxTotalCount(0,$totaluserscount);	//Maximum Users on Site //0 is code for site
echo "
$pageuserscount User(s) online on this page<br>
$totaluserscount User(s) online on the Site<br>
$maxpageuserscount on this page<br>
$maxtotaluserscount on the Site";
}

/*
	Create a thumbnail of $srcFile and save it to $destFile.
	The thumbnail will be $width pixels.
*/
function createThumbnail($srcFile, $destFile, $width, $quality = 75)
{
	$thumbnail = '';

	if (file_exists($srcFile)  && isset($destFile))
	{
		$size        = getimagesize($srcFile);
		$w           = number_format($width, 0, ',', '');
		$h           = number_format(($size[1] / $size[0]) * $width, 0, ',', '');

		$thumbnail =  copyImage($srcFile, $destFile, $w, $h, $quality);
	}

	// return the thumbnail file name on sucess or blank on fail
	return basename($thumbnail);
}

/*
	Copy an image to a destination file. The destination
	image size will be $w X $h pixels
*/
function copyImage($srcFile, $destFile, $w, $h, $quality = 75)
{
    $tmpSrc     = pathinfo(strtolower($srcFile));
    $tmpDest    = pathinfo(strtolower($destFile));
    $size       = getimagesize($srcFile);

    if ($tmpDest['extension'] == "gif" || $tmpDest['extension'] == "jpg")
    {
       $destFile  = substr_replace($destFile, 'jpg', -3);
       $dest      = imagecreatetruecolor($w, $h);
       imageantialias($dest, TRUE);
    } elseif ($tmpDest['extension'] == "png") {
       $dest = imagecreatetruecolor($w, $h);
       imageantialias($dest, TRUE);
    } else {
      return false;
    }

    switch($size[2])
    {
       case 1:       //GIF
           $src = imagecreatefromgif($srcFile);
           break;
       case 2:       //JPEG
           $src = imagecreatefromjpeg($srcFile);
           break;
       case 3:       //PNG
           $src = imagecreatefrompng($srcFile);
           break;
       default:
           return false;
           break;
    }

    imagecopyresampled($dest, $src, 0, 0, 0, 0, $w, $h, $size[0], $size[1]);

    switch($size[2])
    {
       case 1:
       case 2:
           imagejpeg($dest,$destFile, $quality);
           break;
       case 3:
           imagepng($dest,$destFile);
    }
    return $destFile;

}

/*
	Create the paging links
*/
function getPagingNav($sql, $pageNum, $rowsPerPage, $queryString = '')
{
	$result  = mysql_query($sql) or die('Error, query failed. ' . mysql_error());
	$row     = mysql_fetch_array($result, MYSQL_ASSOC);
	$numrows = $row['numrows'];

	// how many pages we have when using paging?
	$maxPage = ceil($numrows/$rowsPerPage);

	$self = $_SERVER['PHP_SELF'];

	// creating 'previous' and 'next' link
	// plus 'first page' and 'last page' link

	// print 'previous' link only if we're not
	// on page one
	if ($pageNum > 1)
	{
		$page = $pageNum - 1;
		$prev = " <a href=\"$self?page=$page{$queryString}\">[Prev]</a> ";

		$first = " <a href=\"$self?page=1{$queryString}\">[First Page]</a> ";
	}
	else
	{
		$prev  = ' [Prev] ';       // we're on page one, don't enable 'previous' link
		$first = ' [First Page] '; // nor 'first page' link
	}

	// print 'next' link only if we're not
	// on the last page
	if ($pageNum < $maxPage)
	{
		$page = $pageNum + 1;
		$next = " <a href=\"$self?page=$page{$queryString}\">[Next]</a> ";

		$last = " <a href=\"$self?page=$maxPage{$queryString}{$queryString}\">[Last Page]</a> ";
	}
	else
	{
		$next = ' [Next] ';      // we're on the last page, don't enable 'next' link
		$last = ' [Last Page] '; // nor 'last page' link
	}

	// return the page navigation link
	return $first . $prev . " Showing page <strong>$pageNum</strong> of <strong>$maxPage</strong> pages " . $next . $last;
}


/*
	Upload an image and return the uploaded image name
*/
function uploadProductImage($inputName, $uploadDir)
{
	$image     = $_FILES[$inputName];
	$imagePath = '';
	$thumbnailPath = '';

	// if a file is given
	if (trim($image['tmp_name']) != '') {
		$ext = substr(strrchr($image['name'], "."), 1); //$extensions[$image['type']];

		// generate a random new file name to avoid name conflict
		$imagePath = md5(rand() * time()) . ".$ext";

		list($width, $height, $type, $attr) = getimagesize($image['tmp_name']);

		// make sure the image width does not exceed the
		// maximum allowed width

		if (LIMIT_USER_WIDTH && $width > MAX_USER_IMAGE_WIDTH) {
			$result    = createThumbnail($image['tmp_name'], $uploadDir . $imagePath, MAX_USER_IMAGE_WIDTH);
			$imagePath = $result;
		} else {
			$result = move_uploaded_file($image['tmp_name'], $uploadDir . $imagePath);
		}

		if ($result) {
			// create thumbnail
			$thumbnailPath =  md5(rand() * time()) . ".$ext";
			$result = createThumbnail($uploadDir . $imagePath, $uploadDir . $thumbnailPath, THUMBNAIL_WIDTH);

			// create thumbnail failed, delete the image
			if (!$result) {
				unlink($uploadDir . $imagePath);
				$imagePath = $thumbnailPath = '';
			} else {
				$thumbnailPath = $result;
			}
		} else {
			// the product cannot be upload / resized
			$imagePath = $thumbnailPath = '';
		}

	}

	return array('image' => $imagePath, 'thumbnail' => $thumbnailPath);
}

	function childPossible($referral_by){
		$sql = sprintf('select count(*) from tbl_users where referral_by = "%s"',$referral_by);
		$result  = dbFetchArray(dbQuery($sql));

		$output = false;
		// print_r($result);
		if($result[0] <2){
			// printf('parent %s children %s',$referral_by,dbFetchArray($result)[0]);
			$output = true;
		}

		return $output;

	}

	function getNextLeftChild($referral_by){
		$sql = sprintf('select * from tbl_users where referral_by = "%s" order by id ASC LIMIT 1',$referral_by);

		$result  = dbFetchAssoc(dbQuery($sql));
		// print($result['referral_by']);
		if(childPossible($referral_by)){
			//printf('%s can be parent<br>',$referral_by);
			return $referral_by;
		}else{
			//printf('%s cant be parent<br>',$referral_by);
			return getNextLeftChild($result['referral_code']);
		}

	}

	// function getVAlidReferral($referral_by){
	// 	if(childPossible($referral_by)){
	// 		return $referral_by;
	// 	}else{
	// 		return getNextLeftChild($referral_by);
	// 	}
	// }

	function getVAlidReferral($referral_code){
		//get
		$levels = [];

		$levels[] = [];

		if(!childPossible($referral_code)){
			//get children for first level
			addChildrenToLevel($referral_code,$levels[0]);

			// first array ready now step through elements
			return findPosibleParentFromLevel($levels,0);
		}else{
			return $referral_code;
		}


		print(json_encode($levels));




	}

	function findPosibleParentFromLevel($levels,$level_key){


			$levels[$level_key + 1] = [];
			$level = $levels[$level_key];//level is actually children in particular level

			foreach ($level as $child) {
					// add children of child to next_level
					addChildrenToLevel($child['referral_code'],$levels[$level_key + 1]);
				}

			foreach ($level as $child) {
				if(childPossible($child['referral_code'])){
					// echo  $child['referral_code'];
					return  $child['referral_code'];
				}
			}
		return findPosibleParentFromLevel($levels,$level_key + 1);
	}

	 function addChildrenToLevel($referral_code,&$level_array){
			$sql = sprintf('select * from tbl_users where referral_by="%s"',$referral_code);
		 $result = dbQuery($sql);
		 while($row = dbFetchAssoc($result)){
			 array_push($level_array,$row);
		 }
	 }

	 function getFirstUserReferralCode(){
		$sql = "select * from tbl_users order by id asc limit 1";
		$result = dbQuery($sql);
			if($result){
				return dbFetchAssoc($result)['referral_code'];
			}else{
				return '';
			}
		}

		function getAParent($request_arr){
			if(!isset($request_arr['referral_by']) || $request_arr['referral_by'] == ''){
				return getFirstUserReferralCode();
			}else{
				return $request_arr['referral_by'];
			}
		}

		function getUpgradeRequest($user_id){
			$sql = 'select req.*,usr.*, acc.level from upgrade_requests as req,(select tbl_users.* from tbl_users) as usr,(select tbl_accounts.* from tbl_accounts) as acc where req.user_id = usr.id and acc.user_id = usr.id and req.parent_id ='.$_SESSION['hlbank_user']['id'] ;
			$result = dbQuery($sql);
			$result_arr = [];

			while($row = dbFetchAssoc($result)){
				array_push($result_arr,$row);
			}

			return $result_arr;
		}

		function getDecendants($referral_code,&$nodes){
      // printf('// referral_code %s ',$referral_code);
      //decendants sql
      $sql = sprintf('select tbl_users.*,acc.level from tbl_users, (select * from tbl_accounts) as acc where referral_by = "%s" and tbl_users.id = acc.id',$referral_code);
      $result_set = dbQuery($sql);

      while($result = dbFetchAssoc($result_set)){

          array_push($nodes,$result);
          getDecendants($result['referral_code'],$nodes);
      }

    }

		function getParent($referral_by,&$nodes){
      //add parent
       $sql = sprintf('select * from tbl_users where referral_code = "%s"',$referral_by);
       $result = dbFetchAssoc(dbQuery($sql));

       //add value of result if it contains an element
       if($result)
         array_unshift($nodes,$result);

       if(strlen($result['referral_by']) != 0 ){
         getParent($result['referral_by'],$nodes);
       }
    }

		function isReferral_byValid($referral_by){
			$sql = sprintf('Select count(*) as count from tbl_users where referral_code = "%s"',$referral_by);

			return (int)dbFetchAssoc(dbQuery($sql))['count'];
		}


		$required_charges_by_level = array('Level 1'=> '50','Level 2'=> '80','Level 3'=>'200');

		function add_testimonial($user_id,$message){
	    $sql = sprintf('insert into testimonials (user_id,message,approval) values(%s,"%s",false);', $user_id, $message);
	      $result = dbQuery($sql);
	      if($result){
	      $subject = "New Testimonial Posted";
		  $to = "ghanadonorclub@gmail.com";
          $mailbody = "Dear Administrator,<br><br> New Testimonial has being posted. Kindly review to approve or decline.<br><br> Thank you";
          $mail_data = array('to' => $to, 'sub' => $subject, 'msg' => 'testimonials', 'testimonials' => $mailbody);
	      send_email($mail_data);
	        header('Location: ' . WEB_ROOT . 'view?v=testimonials&flash=true');
	      }else{
	        header('Location: ' . WEB_ROOT . 'view?v=testimonials&flash=false');

	      }
	  }

		//$required_charges_by_level = array('Level 1'=> '50','Level 2'=> '70','Level 3'=>'200');

		// define('REQUIRED_CHARGES_BY_LEVEL', array('Level 1'=> '20','Level 2'=> '80','Level 3'=>'200'));

		function notify_user_upgrade_to_next_level($id){
			$sql = sprintf('select tbl_accounts.level from tbl_accounts where tbl_accounts.user_id=%s limit 1',$id);
			$target_level = dbFetchAssoc(dbQuery($sql))['level'];

			if($target_level == 'Free user'){
				$sql = sprintf('select count(*) as count from upgrade_requests where parent_id=%s ;',$id);
				$should_notify = dbFetchAssoc(dbQuery($sql))['count'] != 0;//check if parent has upgraded both children to his own level
				$next_level = 'Level 1 <b> immediately</b>';
				return $next_level;
			}

			$sql = sprintf('select count(*) as count from upgrade_requests where parent_id=%s and target_level="%s" and approval="approved" ;',$id,$target_level);
			$should_notify = dbFetchAssoc(dbQuery($sql))['count'] == 2;//check if parent has upgraded both children to his own level
			$next_level = null;
			if($should_notify)
				switch ($target_level) {
					case 'Level 1':
						$next_level = 'Level 2';
						break;

					//  case 'Level 2':
					// 	$next_level = 'Level 3';
					// 	break;

					//  case 'Level 3':
					// 	$next_level = 'You are don';
					// 	break;

					default:

				    break;
				};

				return $next_level;
		}





		//disable user if has not requested for upgrade in 3 hours
		function disable_on_compel_fail($user_id,$next_level){

			$result = null;
			if(isset($next_level)){
				// find if new upgrade req has been made by user older than compelledupgrade time that is not null
					$sql = sprintf('select compel_upgrade_time from tbl_users where id=%s',$user_id);
					if(!($compel_upgrade_result = dbFetchAssoc(dbQuery($sql)))){
						return $result;
					}
					$compel_upgrade_time = $compel_upgrade_result['compel_upgrade_time'];

					if(!(strlen($compel_upgrade_time) > 0)){
						// if compel time is not set then set it
						$sql = sprintf('update tbl_users set compel_upgrade_time=CURRENT_TIMESTAMP where id=%s',$user_id);
						dbQuery($sql);
					}else {
						//if no new upgrade req exist and time is past 3 hours

						$sql = sprintf('select count(*) as count from upgrade_requests where upgrade_requests.user_id=%s and updated_at > "%s"',$user_id,$compel_upgrade_time);
						$count = dbFetchAssoc(dbQuery($sql))['count'] == 0;

						if($count){
							//no new request exits then check time equals or exceed 3 hours
							$now = new DateTime('now');
							$datetime1 = new DateTime($compel_upgrade_time);
							$interval = $now->diff($datetime1);
						  $hours = $interval->format('%h');

							if($hours >=3){
								//disable user
								 $sql = sprintf('update tbl_users set is_active="FALSE" where id=%s',$user_id);
								dbQuery($sql);
							}else{
								$_minutes_for_three_hours = 60 * 3;
								$result = $_minutes_for_three_hours - ( ($interval->format('%h') *60) + $interval->format('%i'));//set minutes remaining

							}
						}
					}
			}

			return $result;


		}

		/**
		 * [forceTestimonialInputPopUp description]
		 * @method forceTestimonialInputPopUp
		 * @param  [type]                     $user_id [defults to current user id if not specified]
		 * @return [type]                              [boloean]
		 */
		function showTestimonialInputPopUp($user_id){
			$result = false;

			$sql = sprintf('select level!="Free user" as level from tbl_accounts where user_id = %s', $user_id);
			$level = (boolean) (dbFetchAssoc(dbQuery($sql))['level'] );


			$sql = sprintf('select level from tbl_accounts where user_id=%s and level in ("Level 1","Level 2") limit 1;', $user_id );


				$result = (boolean)(dbFetchAssoc(dbQuery($sql))['count']);
				if($result){
					//print 'show is true';
				}else{
					//print 'show: false';
			if($level = dbFetchAssoc(dbQuery($sql))['level']){
				//level is not Free user
				// print $level['level'];?

				   switch($level){
					case 'Level 1': {
						//check user has approved two level 1 request
						  $sql = sprintf('select count(*) = 2 as prompt from upgrade_requests where parent_id = %s and target_level="Level 1" and approval="approved";', $user_id);

						 if(dbFetchAssoc(dbQuery($sql))['prompt']){
							  $sql = sprintf('select count(*) < 1 as prompt from testimonials where user_id= %s', $user_id);

							 if(dbFetchAssoc(dbQuery($sql))['prompt']){
								 $result = true;

							 }

						 }

						break;
					}
					//case level ends hehe
					case 'Level 2':{
						 $sql = sprintf('select count(*) = 2 as prompt from upgrade_requests where parent_id = %s and target_level="Level 2" and approval="approved";', $user_id);

						if(dbFetchAssoc(dbQuery($sql))['prompt']){
							 $sql = sprintf('select count(*) < 2 as prompt from testimonials where user_id= %s', $user_id);

							if(dbFetchAssoc(dbQuery($sql))['prompt']){
								//set is_active to false and status to completed;
								 $sql = sprintf('update tbl_accounts set status="completed" where id=%s', $user_id);
								dbQuery($sql);
								 $sql = sprintf(' update tbl_users set is_active=False where id=%s;', $user_id);
								dbQuery($sql);

								$result = true;

							}

						}
						break;
					}
					//case level 2 ends
				}

			}

			return $result;


		}
	}

?>
