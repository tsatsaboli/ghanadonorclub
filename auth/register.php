<?php
require_once './library/config.php';
require_once './library/functions.php';

$errorMessage = '';
if (isset($_POST['submitButton'])) {
  $result = doRegister();
  if ($result != '') {
    $errorMessage = $result;
  }
}

if(isset($_GET['ref'])){

  $Id = $_GET['ref'];

  $result = dbQuery("SELECT * FROM tbl_users u, tbl_accounts a WHERE u.id = a.user_id AND u.referral_code='$Id'");
  $row = dbFetchAssoc($result);
  $alertMessage='<div class="callout callout-info" style="text-align: center">
  <p>Welcome.! You were invited by [ '. $row['fullname'] .' ]</p>
  </div>';
}else{
  $alertMessage='
  <div class="alert alert-danger" style="text-align: center">
  <p>You must Use a Sponsor / Referral Link to Partake in this Program. Fill the form if you have a Sponsor Username</p>
  </div>';
}
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Registration Page</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="plugins/iCheck/square/blue.css">


  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  style
  <![endif]-->
  <style type="text/css">.squaredFour {
    width: 20px;
    position: relative;
    margin: 20px auto;
    label {
      width: 20px;
      height: 20px;
      cursor: pointer;
      position: absolute;
      top: 0;
      left: 0;
      background: #fcfff4;
      background: linear-gradient(top, #fcfff4 0%, #dfe5d7 40%, #b3bead 100%);
      border-radius: 4px;
      box-shadow: inset 0px 1px 1px white, 0px 1px 3px rgba(0,0,0,0.5);
      &:after {
        content: '';
        width: 9px;
        height: 5px;
        position: absolute;
        top: 4px;
        left: 4px;
        border: 3px solid #333;
        border-top: none;
        border-right: none;
        background: transparent;
        opacity: 0;
        transform: rotate(-45deg);
      }
      &:hover::after {
        opacity: 0.5;
      }
    }
    input[type=checkbox] {
      visibility: hidden;
      &:checked + label:after {
        opacity: 1;
      }
    }
  }</style>
</head>
<body class="hold-transition register-page">
  <div class="register-box">
    <div class="login-logo">
      <a href=""><b>Ghana Donor Club</b></a>
    </div>
    <div id="my-modal" class="modal fade">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title" style="text-align: center;">Warning</h4>
          </div>
          <div class="modal-body">
            <p><?php echo $alertMessage?></p>
            <p style="text-align: center;">Please we advise that you have your initial capital with you as the system has given you a maximum of [6 hours] to make your payment otherwise you will be removed completely. Thank you</p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-primary" data-dismiss="modal" aria-hidden="true">Close</button></div>
          </div>
        </div>
      </div>
      <div class="register-box-body">
        <p class="login-box-msg">Register a new membership</p>

        <p style="text-align: center;"><?php echo $errorMessage?></p>
        <form action="" method="post" enctype="multipart/form-data" id="acclogin">
          <div class="form-group has-feedback">
            <input type="text" class="form-control" placeholder="Full name" name="fullname" required value="<?php if( isset($_GET['ref_err']) && $_GET['ref_err']){ print( $_GET['fullname']);}?>">

          </div>
          <div class="form-group has-feedback">
            <input type="email" class="form-control" placeholder="Email" name="email" required value="<?php if( isset($_GET['ref_err']) && $_GET['ref_err']){ print( $_GET['email']);}?>">

          </div>
          <div class="form-group has-feedback">
            <input type="text" class="form-control" placeholder="Phone Number" name="phone" required value="<?php if( isset($_GET['ref_err']) && $_GET['ref_err']){ print( $_GET['phone']);}?>">

          </div>
          <div class="form-group">
            <select class="form-control" name="country" value="<?php if( isset($_GET['ref_err']) && $_GET['ref_err']){ print( $_GET['country']);}?>">
              <option>Select Country</option>
              <option value="Ghana">Ghana</option>
            </select>
          </div>
          <div class="form-group has-feedback">
            <input type="text" class="form-control" placeholder="City" name="city" value="<?php if( isset($_GET['ref_err']) && $_GET['ref_err']){ print( $_GET['city']);}?>" required>

          </div>
          <div class="form-group">

            <select class="form-control" name="gender" required value="<?php if( isset($_GET['ref_err']) && $_GET['ref_err']){ print( $_GET['gender']);}?>">
              <option>Select Gender</option>
              <option value="Male">Male</option>
              <option value="Female">Female</option>
            </select>
          </div>
          <div class="form-group has-feedback">
            <input type="text" class="form-control" placeholder="Username" name="pin" required value="<?php if( isset($_GET['ref_err']) && $_GET['ref_err']){ print( $_GET['pin']);}?>">

          </div>
          <div class="form-group has-feedback">
            <input type="password" class="form-control" placeholder="Password" name="password" required='true'>

          </div>
          <div class="form-group">

            <select class="form-control" id="colorselector" name="paymentmode" required value="<?php if( isset($_GET['ref_err']) && $_GET['ref_err']){ print( $_GET['paymentmode']);}?>">
              <option>Select Payment Mode</option>
              <option value="MobileMoney">Mobile Money</option>
            </select>
            <div id="MobileMoney" class="MobileMoney" style="display:none"> <div class="box-body">
              <label>
                <input name="acctype" type="checkbox" value="MTN" id="acctype">MTN &nbsp
                <input name="acctype" type="checkbox" value="Vodafon" id="acctype">Vodafon &nbsp
                <input name="acctype" type="checkbox" value="Tigo" id="acctype">Tigo &nbsp
                <input name="acctype" type="checkbox" value="Airtel" id="acctype">Airtel
              </label>
              <div class="row">
                <div class="col-xs-6">
                  <input type="text" class="form-control" placeholder="MM Name" name="mMName" value="<?php if( isset($_GET['ref_err']) && $_GET['ref_err']){ print( $_GET['mMName']);}?>" required>
                </div>
                <div class="col-xs-6">
                  <input type="text" class="form-control" placeholder="MM No." name="mMNumber" value="<?php if( isset($_GET['ref_err']) && $_GET['ref_err']){ print( $_GET['mMNumber']);}?>" required>
                </div>
              </div>
            </div></div>
          </div>

          <!-- referral_by input -->
          <div class="form-group has-feedback">
            <div class="">
              <?php if(isset($_GET['ref_err']) && $_GET['ref_err']){ ?>

                <div class="alert alert-dang">

                </div>
              <?php }
              // if(isset($_GET['ref'])){
              printf('<input class="form-control" type="text" placeholder="Provide referral code if available" name="referral_by" value="%s" required>',$_GET['ref']);
              // }
              ?>
            </div>
          </div>
          <!-- end referral_by -->
          <div class="row">
            <div class="col-xs-8">
              <div class="checkbox icheck" style="padding-left: 16px;">
                <label>
                  <input name="agree" type="checkbox" value="yes" id="agree" class="squaredFour" required value="<?php if( isset($_GET['ref_err']) && $_GET['ref_err']){ print( $_GET['agree']);}?>"> I agree to the terms
                </label>
              </div>
            </div>
            <!-- /.col -->
            <div class="col-xs-8">

              <button type="submit" class="btn btn-primary btn-block btn-flat popup-trigger center" id="submitButton" name="submitButton">Register</button>
            </div>
            <!-- /.col -->
          </div>
        </form>

        <a href="login.php" class="text-center">I already have a membership</a>
      </div>
      <!-- /.form-box -->
          <p style="color: red"><marquee>For all Technical Support, Contact us on +233558749927</marquee></p>
    </div>
    <!-- Modal Window content -->
    <div id="myModal" class="modal fade" tabindex="-1" role="dialog">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h3>Warning</h3>
          </div>
          <div class="modal-body">
            <p>Please we advise that you have your initial capital with you as the system has given you a maximum of [6 hours] to make your payment otherwise you will be removed completely. Thank you</p>
          </div>
          <div class="modal-footer">
            <button class="btn" data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>
    <!-- /.register-box -->

    <!-- jQuery 2.2.3 -->
    <script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
    <!-- Bootstrap 3.3.6 -->
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <!-- iCheck -->
    <script src="plugins/iCheck/icheck.min.js"></script>


    <script>
      $(function() {    // Makes sure the code contained doesn't run until
      //     all the DOM elements have loaded

      $('#colorselector').change(function(){
        $('.MobileMoney').hide();
        $('#' + $(this).val()).show();
      });

      $('#my-modal').modal('show');
    });

    var but=$('#submitButton');
    but.attr('disabled','disabled');
    $('#agree').change(function(e){
      if(this.checked)
      {
        but.removeAttr('disabled');
      }
      else
      {
        but.attr('disabled','disabled')
      }
    });
  </script>
</body>
</html>
