<div class="col-md-12">
  <div class="box-header with-border">
    <h3 class="box-title">Upgrade</h3>
  </div>
  <!-- Custom Tabs -->
  <div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
      <li class="active"><a href="#tab_3" data-toggle="tab">All Upgrade Request</a></li>
    </ul>
    <div class="tab-content">
      <!-- /.tab-pane -->
      <div class="tab-pane active" id="tab_3">
        <div class="row">
          <div class="box-body">
            <p>This is for record keeping, which allows you to keep track of your downlines whom you upgraded.</p>

          </div>
          <table class="table">
            <tbody>
              <?php $requests =getUpgradeRequest($_SESSION['hlbank_user']);$i=1;  ?>

              <?php foreach($requests as $request) {?>
                <?php if($request['approval'] == 'approved'){ ?>
                  <tr>
                    <td><span class="fa fa-bullseye"></td>
                  </tr>
                  <tr>
                    <td>Full Name</td>
                    <td><?php echo $request['fullname']; ?></td>
                  </tr>
                  <tr>
                    <td>User(s) Level</td>
                    <td><?php echo $request['level']; ?></td>
                  </tr>
                  <tr>
                    <td>Level Upgrading to</td>
                    <td><?php echo $request['target_level']; ?></td>
                  </tr>
                  <tr>
                    <td>Status</td>
                    <td><?php echo $request['approval']; ?></td>
                  </tr>
                  <!-- <tr><td colspan="2"><button type="button" name="button" class="btn btn-primary">Next Request</button></td></tr> -->
                  <?php  }} ?>
                </tbody>
              </table>
            </div>
          </div>
          <!-- /.tab-pane -->
        </div>
        <!-- /.tab-content -->
      </div>
      <!-- nav-tabs-custom -->
    </div>
