       <form action="" method="post">
              <div class="box-header with-border">
              <h3 class="box-title">Compose New Message</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
            <p>You can get intouch with us here or call our mobile number and we will be glad to help you out immediately</p>
              <div class="form-group">
              <h2>How may we help you?</h2>
                 <textarea id="" class="form-control" style="height: 300px;" name="messageBox">             
                </textarea>
              </div>

            </div>
            <!-- /.box-body -->
            <div class="box-footer">
              <div class="pull-right">
                <button type="submit" class="btn btn-primary" name="customerSendBtn"><i class="fa fa-envelope-o"></i> Send</button>
              </div>
             
            </div>

       </form>