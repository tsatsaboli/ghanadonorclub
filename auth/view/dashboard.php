<?php
  $children = [];
  getDecendants($_SESSION['hlbank_user']['referral_code'],$children);

  $spillovers = 0;
  foreach ($children as $child) {
    if($child['original_referral_by'] == $_SESSION['hlbank_user']['referral_code'] && $child['original_referral_by'] != $child['referral_by'])
    $spillovers++;
  }

  //get pending requests;
  $sql = sprintf('select count(*) as count from upgrade_requests where approval="pending" and parent_id=%s',$_SESSION['hlbank_user']['id']);
  $result = dbFetchAssoc(dbQuery($sql));

  $requests_pending = $result['count'];

 ?>
<div class="col-md-12">
          <!-- Custom Tabs -->
          <div class="box-header with-border">
          <h3 class="box-title">Dashboard</h3><br>

        </div>
        <h2 class="text-green" style="text-align: center;"><span class="greeting"></span> <strong><?php echo $username .' '. '['.$usr.']'?></strong></h2>
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#tab_1" data-toggle="tab">Account</a></li>
              <li><a href="#tab_2" data-toggle="tab">Network</a></li>
              <li><a href="#tab_3" data-toggle="tab">Earnings</a></li>
              <li><a href="#tab_4" data-toggle="tab">My Downline Activities</a></li>
              <li><a href="#tab_5" data-toggle="tab">Info Box</a></li>
            </ul>
            <div class="tab-content">
              <div class="tab-pane active" id="tab_1">
               <div class="row">

               <div class="box-body">
              <ul class="todo-list ui-sortable">
                <li class="external-event bg-green">
                  <!-- todo text -->
                  <span class="text">Today's Date is: <?php echo $date = date('Y-m-d'); ?></span>
                  <!-- Emphasis label -->
                </li><br>
                 <li class="external-event bg-yellow">
                  <!-- todo text -->
                  <span class="text">My current level is: <?php echo $usrLevel ?></span>
                  <!-- Emphasis label -->
                </li><br>
                <li class="external-event bg-light-blue">
                  <!-- todo text -->
                  <span class="text">My User ID is :  <?php echo $usrID;?> ( please use this for any complaints )</span>
                  <!-- Emphasis label -->
                </li><br>
                 <li class="external-event bg-red">
                  <!-- todo text -->
                  <span class="text">Hey <span class="text-lime"><strong><?php echo $usr;?></strong></span>, It's time to get more Downline(s). Share your Link to grow your network</span>
                  <!-- Emphasis label -->
                </li>
              </ul>

            </div>
        <div class="col-lg-4 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3><?php echo $spillovers ?></h3>

              <p>Spill Overs (In your group)</p>
            </div>
          
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-4 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3><?php echo count($children) ?></h3>

              <p>Child Downline (In your group)</p>
            </div>
            
            <a href="<?php echo WEB_ROOT;?>view/?v=downlines" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <!-- ./col -->
        <div class="col-lg-4 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3><?php echo $requests_pending ?></h3>

              <p>Upgrade Request</p>
            </div>
           
            <a href="<?php echo WEB_ROOT; ?>view/?v=upgraderequest" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>

        <!-- ./col -->
      </div>
              </div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="tab_2">
                <div class="row">
               <div class="box-body">
              <p>You can simply share your link to friends and other people in other to grow your downline</p>
              <p>Your referral Link is: <span class="text-fuchsia"><strong><?php echo $refLink;?></strong></span></p>
                  <ul class="todo-list ui-sortable">
                <li class="external-event bg-green">
                  <!-- todo text -->
                  <span class="text">Today's Date is: <?php echo $date = date('Y-m-d'); ?></span>
                  <!-- Emphasis label -->
                </li><br>
                 <li class="external-event bg-yellow">
                  <!-- todo text -->
                  <span class="text">My current level is: <?php echo $usrLevel ?></span>
                  <!-- Emphasis label -->
                </li><br>
                <li class="external-event bg-light-blue">
                  <!-- todo text -->
                  <span class="text">My User ID is :  <?php echo $usrID;?> ( please use this for any complaints )</span>
                  <!-- Emphasis label -->
                </li><br>
                 <li class="external-event bg-red">
                  <!-- todo text -->
                  <span class="text">Hey <span class="text-lime"><strong><?php echo $usr;?></strong></span>, It's time to get more Downline(s). Share your Link to grow your network</span>
                  <!-- Emphasis label -->
                </li>
              </ul>
            </div>
         <div class="col-lg-4 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3><?php echo $spillovers ?></h3>

              <p>Spill Overs (In your group)</p>
            </div>
          
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-4 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3><?php echo count($children) ?></h3>

              <p>Child Downline (In your group)</p>
            </div>
            
            <a href="<?php echo WEB_ROOT;?>view/?v=downlines" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <!-- ./col -->
        <div class="col-lg-4 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3><?php echo $requests_pending ?></h3>

              <p>Upgrade Request</p>
            </div>
           
            <a href="<?php echo WEB_ROOT; ?>view/?v=upgraderequest" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
      </div>
              </div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="tab_3">
                <div class="row">
               <div class="box-body">
               <p>You are expected to Earn the following through your journey here.</p>
               <li class="bg-default"><span class="pull-left">[0] Free User</span> <span class="pull-right">0.00</span></li><br>
               <li class="bg-aqua"><span class="pull-left">[1] Silver(LEVEL 1)</span> <span class="pull-right">30.00</span></li><br><li class="bg-blue"><span class="pull-left">[2] Gold(LEVEL 2)</span> <span class="pull-right">80.00</span></li><br><li class="bg-blue"><span class="pull-left">[2] Diamond(LEVEL 3)</span> <span class="pull-right">1,600.00</span></li><br>
                <ul class="todo-list ui-sortable">
                <li class="external-event bg-green">
                  <!-- todo text -->
                  <span class="text">Today's Date is: <?php echo $date = date('Y-m-d'); ?></span>
                  <!-- Emphasis label -->
                </li><br>
                 <li class="external-event bg-yellow">
                  <!-- todo text -->
                  <span class="text">My current level is: <?php echo $usrLevel ?></span>
                  <!-- Emphasis label -->
                </li><br>
                <li class="external-event bg-light-blue">
                  <!-- todo text -->
                  <span class="text">My User ID is :  <?php echo $usrID;?> ( please use this for any complaints )</span>
                  <!-- Emphasis label -->
                </li><br>
                 <li class="external-event bg-red">
                  <!-- todo text -->
                  <span class="text">Hey <span class="text-lime"><strong><?php echo $usr;?></strong></span>, It's time to get more Downline(s). Share your Link to grow your network</span>
                  <!-- Emphasis label -->
                </li>
              </ul>
            </div>
        <div class="col-lg-4 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3><?php echo $spillovers ?></h3>

              <p>Spill Overs (In your group)</p>
            </div>
          
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-4 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3><?php echo count($children) ?></h3>

              <p>Child Downline (In your group)</p>
            </div>
            
            <a href="<?php echo WEB_ROOT;?>view/?v=downlines" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <!-- ./col -->
        <div class="col-lg-4 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3><?php echo $requests_pending ?></h3>

              <p>Upgrade Request</p>
            </div>
           
            <a href="<?php echo WEB_ROOT; ?>view/?v=upgraderequest" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
      </div>
              </div>
               <div class="tab-pane" id="tab_4">
                <div class="row">
               <div class="box-body">

              <p>There is no activity for you yet... </p>
                   <ul class="todo-list ui-sortable">
                <li class="external-event bg-green">
                  <!-- todo text -->
                  <span class="text">Today's Date is: <?php echo $date = date('Y-m-d'); ?></span>
                  <!-- Emphasis label -->
                </li><br>
                 <li class="external-event bg-yellow">
                  <!-- todo text -->
                  <span class="text">My current level is: <?php echo $usrLevel ?></span>
                  <!-- Emphasis label -->
                </li><br>
                <li class="external-event bg-light-blue">
                  <!-- todo text -->
                  <span class="text">My User ID is :  <?php echo $usrID;?> ( please use this for any complaints )</span>
                  <!-- Emphasis label -->
                </li><br>
                 <li class="external-event bg-red">
                  <!-- todo text -->
                  <span class="text">Hey <span class="text-lime"><strong><?php echo $usr;?></strong></span>, It's time to get more Downline(s). Share your Link to grow your network</span>
                  <!-- Emphasis label -->
                </li>
              </ul>
            </div>
 <div class="col-lg-4 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3><?php echo $spillovers ?></h3>

              <p>Spill Overs (In your group)</p>
            </div>
          
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-4 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3><?php echo count($children) ?></h3>

              <p>Child Downline (In your group)</p>
            </div>
            
            <a href="<?php echo WEB_ROOT;?>view/?v=downlines" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <!-- ./col -->
        <div class="col-lg-4 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3><?php echo $requests_pending ?></h3>

              <p>Upgrade Request</p>
            </div>
           
            <a href="<?php echo WEB_ROOT; ?>view/?v=upgraderequest" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
      </div>
              </div>
               <div class="tab-pane" id="tab_5">
                <div class="row">
               <div class="box-body">
               <p>Please don't request for an <span class="text-aqua">UPGRADE</span> if you are <span class="text-blue">NOT</span> ready for the <span class="text-light-blue">UPGRADE</span>.Once any request is sent, <span class="text-teal">NO</span> upliner should <span class="text-yellow">CANCEL</span> any request for any reason unless you do that at your <span class="text-orange">OWN</span> expense. If you request for <span class="text-green">UPGRADE</span> cancellation and your <span class="text-lime">UPLINER</span> reports you then your account would be <span class="text-red">BLOCKED</span> Thank You. </p>
               <p>
<a href="<?php echo WEB_ROOT;?>view/?v=downlines"><button type="button" class="btn bg-maroon btn-flat margin">My Downlines</button></a>
                <a href=""><button type="button" class="btn bg-purple btn-flat margin">My Downlines Info</button></a>
                <a href="<?php echo WEB_ROOT;?>view/?v=upgraderequest"><button type="button" class="btn bg-navy btn-flat margin">You have a pending Request</button></a>
              </p>
                <ul class="todo-list ui-sortable">
                <li class="external-event bg-green">
                  <!-- todo text -->
                  <span class="text">Today's Date is: <?php echo $date = date('Y-m-d'); ?></span>
                  <!-- Emphasis label -->
                </li><br>
                 <li class="external-event bg-yellow">
                  <!-- todo text -->
                  <span class="text">My current level is: <?php echo $usrLevel ?></span>
                  <!-- Emphasis label -->
                </li><br>
                <li class="external-event bg-light-blue">
                  <!-- todo text -->
                  <span class="text">My User ID is :  <?php echo $usrID;?> ( please use this for any complaints )</span>
                  <!-- Emphasis label -->
                </li><br>
                 <li class="external-event bg-red">
                  <!-- todo text -->
                  <span class="text">Hey <span class="text-lime"><strong><?php echo $usr;?></strong></span>, It's time to get more Downline(s). Share your Link to grow your network</span>
                  <!-- Emphasis label -->
                </li>
              </ul>
            </div>
 <div class="col-lg-4 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3><?php echo $spillovers ?></h3>

              <p>Spill Overs (In your group)</p>
            </div>
          
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-4 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3><?php echo count($children) ?></h3>

              <p>Child Downline (In your group)</p>
            </div>
            
            <a href="<?php echo WEB_ROOT;?>view/?v=downlines" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <!-- ./col -->
        <div class="col-lg-4 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3><?php echo $requests_pending ?></h3>

              <p>Upgrade Request</p>
            </div>
           
            <a href="<?php echo WEB_ROOT; ?>view/?v=upgraderequest" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
      </div>
              </div>
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- nav-tabs-custom -->
        </div>
