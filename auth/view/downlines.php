   <link rel="stylesheet" href="Treant.css">
   <link rel="stylesheet" href="basic-example.css">
<div class="col-md-12">
<div class="box-header with-border">
          <h3 class="box-title">Downline</h3>
        </div>
          <!-- Custom Tabs -->
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#tab_1" data-toggle="tab">My Downlines</a></li>
              <li><a href="#tab_2" data-toggle="tab">My Downlines Generation Tree</a></li>
              <li><a href="#tab_3" data-toggle="tab">My SpillOver(s) here</a></li>
            </ul>
            <div class="tab-content">
              <div class="tab-pane active" id="tab_1">
               <div class="row">
               <div class="box-body"><p>You can view your downline(s) and the 3 downline(s) of each of your direct downline(s) here.</p>
               <div class="chart-responsive" id="basic-example"></div>

    <script src="vendor/raphael.js"></script>
    <script src="Treant.js"></script>

    <!-- <script src="collapsable.js"></script> -->
    <script type="text/javascript">


    <?php

    $nodes = [];



    
    $user = $_SESSION['hlbank_user'];
    $user['referral_by'] ='';
    //get self =
    array_unshift($nodes,$user);

    //add parent
    //  getParent($_SESSION['hlbank_user']['referral_by'],$nodes);


     getDecendants($user['referral_code'],$nodes);

     $template = <<<NODE

 _%s = {
    %s
    //  image: "img/lana.png",
     innerHTML: '<b>%s</b><i> (%s)</i>'
 },

NODE;

     ?>

var config = {
        container: "#basic-example",

        connectors: {
            type: 'step'
        },
        node: {
            HTMLclass: 'nodeExample1'
        }
    },

    <?php
      foreach ($nodes as $node) {
        $referral_by = null;
        if(strlen($node['referral_by']) != 0){
          $referral_by = 'parent: _'. $node['referral_by'] .',';
        }
        printf($template,$node['referral_code'],$referral_by,$node['fullname'],$node['level']);
      }
     ?>

    malory = {
      // parent: _ME4O21Y4,
        image: "img/malory.png",
    },

    lana = {
        parent: malory,
        image: "img/lana.png"
    }

    figgs = {
        parent: lana,
        image: "img/figgs.png"
    }

    sterling = {
        parent: malory,
        childrenDropLevel: 1,
        image: "img/sterling.png"
    },

    woodhouse = {
        parent: sterling,
        image: "img/woodhouse.png"
    },

    pseudo = {
        parent: malory,
        pseudo: true
    },

    cheryl = {
        parent: pseudo,
        image: "img/cheryl.png"
    },

    pam = {
        parent: pseudo,
        image: "img/pam.png"
    },

    chart_config = [
      config,
      //  malory, lana, figgs, sterling, woodhouse, pseudo, pam, cheryl,
      <?php
        foreach ($nodes as $node) {
          print('_'.$node['referral_code'] .',');
        }
       ?>
    ];

    </script>
    <script>
        new Treant( chart_config );
    </script>
</div>




        <!-- ./col -->
      </div>
              </div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="tab_2">
                <div class="row">
                <div class="box-body"><p>Displays your child downline and your child - child downlines and child - child -child downline to the Nth downline in your group</p></div>
      </div>
              </div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="tab_3">
                <div class="row">
              <div class="box-body"> <p>Display Spillovers (i.e users who registered via your link, but cannot be assinged to you because your direct downline is complete. Moreover, they will be assinged to users who have also registered with your link that have incomplete downlines(s) as well. So the train goes on and on</p>
               <p>You do not have a spillover with your link yet</p></div>
      </div>
              </div>

              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- nav-tabs-custom -->
        </div>
