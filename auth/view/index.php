<?php
require_once '../library/config.php';
require_once '../library/functions.php';

$_SESSION['hlbank_return_url'] = $_SERVER['REQUEST_URI'];
checkUser();

$view = (isset($_GET['v']) && $_GET['v'] != '') ? $_GET['v'] : '';

switch ($view) {
	case 'dashboard' :
		$content 	= 'dashboard.php';
		$pageTitle 	= 'Ghana Donor Club - Dashboard';
		break;

		case 'profile' :
		$content 	= 'profile.php';
		$pageTitle 	= 'Ghana Donor Club - Profile';
		break;

		case 'levelremain':
		$content 	= 'levelremain.php';
		$pageTitle 	= 'Ghana Donor Club - level Remain';
		break;

		case 'upgraderequest' :
		$content 	= 'upgraderequest.php';
		$pageTitle 	= 'Ghana Donor Club - Upgrade Request';
		break;

		case 'allrequest' :
		$content 	= 'allrequest.php';
		$pageTitle 	= 'Ghana Donor Club - All Request';
		break;

		case 'downlines' :
		$content 	= 'downlines.php';
		$pageTitle 	= 'Ghana Donor Club - My Downlines';
		break;

		case 'downlinesinfo' :
		$content 	= 'downlinesinfo.php';
		$pageTitle 	= 'Ghana Donor Club - My Downline Info';
		break;

		case 'find_downlines' :
		$content 	= 'find_downlines.php';
		$pageTitle 	= 'Ghana Donor Club - Search My Downlines';
		break;

		case 'earnings' :
		$content 	= 'earnings.php';
		$pageTitle 	= 'Ghana Donor Club - My Earnings';
		break;

		case 'customer_service' :
		$content 	= 'customer_service.php';
		$pageTitle 	= 'Ghana Donor Club - Customer Service';
		break;

		case 'testimonials' :
		$content 	= 'testimonials.php';
		$pageTitle 	= 'Ghana Donor Club - Testimonials';
		break;

		case 'upliner_details' :
		$content 	= 'upliner_details.php';
		$pageTitle 	= 'Ghana Donor Club - My Upliner Details';
		break;


	    default :
		$content 	= 'Dashboard.php';
		$pageTitle 	= 'Ghana Donor Club - Dashboard';
}

$script    = array('category.js');

require_once '../include/template.php';
?>
