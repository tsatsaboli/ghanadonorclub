<?php
  function setUpgradeBtnState($user_id,$target_level){
    $result = getUpgradeBtnStateByLevel($user_id,$target_level);
    if(isUpgradePending($user_id)){
      $result = ' disabled="true" data-pending="true" ';
    }
    return $result;
  }

  function isUpgradePending($user_id){
    $sql = sprintf('select count(*) as count from upgrade_requests where approval ="pending" and user_id=%s',$user_id);
    $result = dbFetchAssoc(dbQuery($sql))['count'] != "0";
    // print($result);
    return $result;
  }

  function getUpgradeBtnStateByLevel($user_id,$target_level){
    // $levels = [
    //   'Free user',
    //   'Level 1',
    //   'Level 2',
    //   'Level 3',
    // ];

    $sql = sprintf('select urs.*,acc.level from tbl_users as urs,(select * from tbl_accounts ) as acc where urs.id=%s and acc.user_id=urs.id limit 1;',$user_id);
    $result = dbFetchAssoc(dbQuery($sql));
    // print($target_level);
    $state = ' ';
    // print($result['level']);
    switch ($result['level']) {
      case 'Free user':
        if($target_level == 'Free user'){
          $state = ' disabled="true" ';
        }
        break;
      case 'Level 1':
        if($target_level =='Level 1'){
          $state = ' disabled="true" ';
        }
        break;
      case 'Level 2':
      //print($target_level);
        if($target_level =='Free user' || $target_level == 'Level 1' || $target_level == 'Level 2'){
          $state = ' disabled="true" ';
        }
        break;
      case 'Level 3':
        if($target_level =='Free user' || $target_level == 'Level 1' || $target_level == 'Level 2' || $target_level == 'Level 3'){
          $state = ' disabled="true" ';
        }
        break;

      default:
        break;
    };
    return $state;
  }
 ?>
<div class="col-md-12">
<div class="box-header with-border">
          <h3 class="box-title">Upgrade</h3>
        </div>
          <!-- Custom Tabs -->
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#tab_1" data-toggle="tab">My Levels Upgrade</a></li>
              <li><a href="#tab_2" data-toggle="tab">Pending Upgrade Request</a></li>
              <li><a href="#tab_3" data-toggle="tab">All Upgrade Request</a></li>
            </ul>
            <div class="tab-content">
              <div class="tab-pane active" id="tab_1">
               <div class="row">
             <div class="box-body">
             <?php
              $lvl = $_SESSION['hlbank_user']['user_id'];
              $sql = "SELECT level FROM tbl_accounts WHERE user_id='$lvl'";
              $result = dbQuery($sql);
              $row = dbFetchAssoc($result);
              $usrLevel = $row['level'];
              ?>
             <p>Note: The system will automatically sort your next upline to upgrade to and which level. So you dont have to worry. Once any upline changes it reflects instantly We ensure this so that you make your payments to the right person</p><br>
             <p>Your current level is: <?php echo $usrLevel?></p><br>

             <div id='hold_btns'>
               <?php
               // if ($usrLevel =="Free user") {
               echo '<a href="" '. setUpgradeBtnState($_SESSION['hlbank_user']['id'],'Level 1') . 'class="btn btn-block btn-social btn-bitbucket" name="l1" onclick="upgradeUser('."'".$_SESSION['hlbank_user']['referral_by']."'".',0,this)">
               Click to upgrade to [1] Silver(LEVEL 1) Please pay GHC '. $required_charges_by_level['Level 1'].' to upliner specified
               </a><br>
               <a href="" '. setUpgradeBtnState($_SESSION['hlbank_user']['id'],'Level 2') .' class="btn btn-block btn-social btn-dropbox" onclick="upgradeUser('."'".$_SESSION['hlbank_user']['referral_by']."'".',1,this)">
               Click to upgrade to [2] Gold(LEVEL 2) Please pay GHC '. $required_charges_by_level['Level 2'].' to upliner specified
               </a><br>';
               //<a href="#" '. setUpgradeBtnState($_SESSION['hlbank_user']['id'],'Level 3') .' class="btn btn-block btn-social btn-foursquare" onclick="upgradeUser('."'".$_SESSION['hlbank_user']['referral_by']."'".',2,this)">
               //Click to upgrade to [3] Diamond(LEVEL 3) Please pay GHC '. $required_charges_by_level['Level 3'].' to upliner specified
               //</a>
               // }
               // elseif ($usrLevel =="Level 1") {
               //   echo '
               //   <a href="#" class="btn btn-block btn-social btn-dropbox" onclick="upgradeUser('."'".$_SESSION['hlbank_user']['referral_by']."'".',1)">
               //      Click to upgrade to [2] Gold(LEVEL 2)
               //   </a><br>
               //   <a href="#" class="btn btn-block btn-social btn-foursquare" onclick="upgradeUser('."'".$_SESSION['hlbank_user']['referral_by']."'".',2)">
               //      Click to upgrade to [3] Diamond(LEVEL 3)
               //   </a>';
               // }elseif ($usrLevel =="Level 2") {
               //   echo '
               //   <a href="#" class="btn btn-block btn-social btn-foursquare" onclick="upgradeUser('."'".$_SESSION['hlbank_user']['referral_by']."'".',2)">
               //      Click to upgrade to [3] Diamond(LEVEL 3)
               //   </a>';
               // }elseif ($usrLevel =="Level 3") {
               //   echo '
               // <a class="btn btn-block btn-social btn-dropbox" onclick="myFunction()">
               //    Congratulations,You have completed your levels. Please start again
               // </a>';
               // }
               ?>

             </div>
          <script type="text/javascript">

              var required_charges_by_level = <?php echo json_encode($required_charges_by_level); ?>;

              function myFunction() {
                    if (confirm('Are you sure you want to REQUEST FOR THIS UPGRADE. Operation is irreversible as Upliner would NOT be allowed to cancel the Request')) {
                    jQuery.ajax({
                        type: "POST",
                        dataType: "json",
                        url: "form.php",
                        async:true,
                        data: {dataToUpdate:$('name').text()}
                    });
                 } else {
                  // Do nothing!
                   }
              }

              function upgradeUser(parent_code,level,obj){
                //check pending
                console.log($(obj).attr('disabled'));
                 if($(obj).attr('data-pending') =='true'){
                  alert('Requesting for upgrade not permitted while previous upgrade request is pending approval');
                  return ;
                }else if($(obj).attr('disabled') == 'disabled'){
                  alert('Attempt to downgrade not permitted');
                  return ;
                }


                var levels_arr= [
                  // 'Free user',
                  'Level 1',
                  'Level 2',
                  'Level 3',
                ];
                //get direct parent of user
                var request_url = `parent_info.php?referral_code=${parent_code}&level=${levels_arr[level]}&user_id=<?php echo $_SESSION['hlbank_user']['id'] ?>`;
                if (confirm('Are you sure you want to REQUEST FOR THIS UPGRADE. Operation is irreversible as Upliner would NOT be allowed to cancel the Request'))
                $.get(request_url).then((response)=>{
                  var parent = JSON.parse(response);
                  var modalChild = `
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-header">
                        <p class="modal-title" style="text-align: center;">You have successfully requested for an upgrade. Below is your upliner information</p>
                      </div>
                      <div class="modal-body">
                        <form class="" action="index.html" method="post">
                          <div class="form-group">
                            <label class="col-md-4" for="name">Fullname</label>
                            <div class="col-md-8">
                              <input type="text" class="form-control" disabled name="name" value="${parent.fullname}">
                            </div>
                          </div>
                                                    <div class="form-group">
                            <label class="col-md-4" for="name">Username</label>
                            <div class="col-md-8">
                              <input type="text" class="form-control" disabled name="name" value="${parent.pin}">
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="col-md-4" for="name">Phone</label>
                            <div class="col-md-8">
                              <input type="text" class="form-control" disabled name="name" value="${parent.phone}">
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="col-md-4" for="name">Level</label>
                            <div class="col-md-8">
                              <input type="text" class="form-control" disabled name="name" value="${parent.level}">
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="col-md-4" for="name">Payment Type</label>
                            <div class="col-md-8">
                              <input type="text" class="form-control" disabled name="name" value="${parent.type}">
                            </div>
                            <div class="form-group">
                                <label class="col-md-4" for="name"> Mobile Money Network</label>
                                <div class="col-md-8">
                                  <input type="text" class="form-control" disabled name="name" value="${parent.acct_type}">
                                </div>
                              </div>
                                                         <div class="form-group">
                            <label class="col-md-4" for="name">Mobile Money Name</label>
                            <div class="col-md-8">
                              <input type="text" class="form-control" disabled name="name" value="${parent.acct_name}">
                            </div>
                          </div>
                            <div class="form-group">
                              <label class="col-md-4" for="name">Mobile Money Number</label>
                              <div class="col-md-8">
                                <input type="text" class="form-control" disabled name="name" value="${parent.acct_number}">
                              </div>
                              <div class="form-group">
                                <label class="col-md-4" for="name">Upgrade fee</label>
                                <div class="col-md-8">
                                  <input type="text" class="form-control" disabled name="name" value="GHC ${required_charges_by_level[parent.target_level]}">
                                </div>
                              </div>
                            </form>
                          </div>
                          <div class="modal-footer">
                            <a href="?v=levelremain"  class="btn btn-primary"  aria-hidden="true">Close</a></div>
                          </div>
                        </div>
                      </div>
                    </div>
                  `;
                  // console.log(modalChild);
                  $('#modalParentInfo').html(modalChild);
                  // $('#modalParentInfo').children().append(modalChild);
                  $('#modalParentInfo').modal();
                },function (err){
                  console.log(err);
                });
              }

              <?php
                //check if user has upgrade req pending
                // print 'checking';
                $sql = sprintf('select acc.type,acc.level,acc.acct_type,acc.acct_name,acc.acct_number,acc.pin,upgrade_requests.approval,tbl_users.*,upgrade_requests.target_level from upgrade_requests left join tbl_accounts as acc on upgrade_requests.parent_id = acc.user_id  join tbl_users on upgrade_requests.parent_id=tbl_users.id where upgrade_requests.user_id=%s  order by upgrade_requests.id desc limit 1',$_SESSION['hlbank_user']['id']);
                $val = dbFetchAssoc(dbQuery($sql));//load logical first element which is actual last upgrad user for user
                // print_r($val);
                if(isset($val) && $val['approval'] == 'pending'){
                  printf("var parent = %s;",json_encode($val));


               ?>
               var modalChild = `
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-header">
                        <p class="modal-title" style="text-align: center;">You have successfully requested for an upgrade. Below is your upliner information</p>
                      </div>
                      <div class="modal-body">
                        <form class="" action="index.html" method="post">
                          <div class="form-group">
                            <label class="col-md-4" for="name">Fullname</label>
                            <div class="col-md-8">
                              <input type="text" class="form-control" disabled name="name" value="${parent.fullname}">
                            </div>
                          </div>
                                                    <div class="form-group">
                            <label class="col-md-4" for="name">Username</label>
                            <div class="col-md-8">
                              <input type="text" class="form-control" disabled name="name" value="${parent.pin}">
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="col-md-4" for="name">Phone</label>
                            <div class="col-md-8">
                              <input type="text" class="form-control" disabled name="name" value="${parent.phone}">
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="col-md-4" for="name">Level</label>
                            <div class="col-md-8">
                              <input type="text" class="form-control" disabled name="name" value="${parent.level}">
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="col-md-4" for="name">Payment Type</label>
                            <div class="col-md-8">
                              <input type="text" class="form-control" disabled name="name" value="${parent.type}">
                            </div>
                            <div class="form-group">
                                <label class="col-md-4" for="name"> Mobile Money Network</label>
                                <div class="col-md-8">
                                  <input type="text" class="form-control" disabled name="name" value="${parent.acct_type}">
                                </div>
                              </div>
                                                         <div class="form-group">
                            <label class="col-md-4" for="name">Mobile Money Name</label>
                            <div class="col-md-8">
                              <input type="text" class="form-control" disabled name="name" value="${parent.acct_name}">
                            </div>
                          </div>
                            <div class="form-group">
                              <label class="col-md-4" for="name">Mobile Money Number</label>
                              <div class="col-md-8">
                                <input type="text" class="form-control" disabled name="name" value="${parent.acct_number}">
                              </div>
                              <div class="form-group">
                                <label class="col-md-4" for="name">Upgrade fee</label>
                                <div class="col-md-8">
                                  <input type="text" class="form-control" disabled name="name" value="GHC ${required_charges_by_level[parent.target_level]}">
                                </div>
                              </div>
                            </form>
                          </div>
                          <div class="modal-footer">
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  `;

               document.getElementById('hold_btns').innerHTML = modalChild;
               console.log(modalChild);
               <?php } ?>

          </script>

             </div>


        <!-- ./col -->
      </div>
              </div>
              <!-- /.tab-pane -->
                          <div class="tab-pane" id="tab_2">
                <div class="row">
                <div class="box-body">
                <p>Kindly approve the following requests from users that have sent you and upgrade fee. Please make sure to verify payments before you confirm as it is irreversible after confirmation</p>
                <div class="table-responsive">
                <div class="box-body">
              <!-- /.table-responsive -->
            </div>
            <table class="table">
              <tbody>
                <?php $requests =getUpgradeRequest($_SESSION['hlbank_user']);$i=1;  ?>

                <?php foreach($requests as $request) {?>
                  <?php if($request['approval'] == 'pending'){ ?>
                  <tr>
              <td>Full Name</td>
              <td><?php echo $request['fullname']; ?></td>
            </tr>
            <tr>
              <td>User(s) Level</td>
              <td><?php echo $request['level']; ?></td>
            </tr>
            <tr>
              <td>Level Upgrading to</td>
              <td><?php echo $request['target_level']; ?></td>
            </tr>
            <tr>
              <td>Status</td>
              <td><?php echo $request['approval']; ?></td>
            </tr>
            <tr><td colspan="2"><button  class="btn btn-sm btn-info btn-flat" onclick="approveDownline(<?php printf("%s,'%s','%s','%s'",$request['user_id'],$request['target_level'],$request['fullname'],$request['approval'])?>)"><i>Approve</i></button>

            <button class="btn btn-sm btn-danger" onclick=""><i>Cancel</i></button></td></tr>
                  <?php  }} ?>
              </tbody>
            </table>
                </div>
             </div>
      </div>
              </div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="tab_3">
                <div class="row">
               <div class="box-body">
               <p>Thid is for record keeping, which allows you to keep track fo your downlines whom you upgraded.</p>

             </div>
             <table class="table">
               <tbody>
                 <?php $requests =getUpgradeRequest($_SESSION['hlbank_user']);$i=1;  ?>

                 <?php foreach($requests as $request) {?>
                   <?php if($request['approval'] == 'approved'){ ?>
                   <tr>
               <td>Full Name</td>
               <td><?php echo $request['fullname']; ?></td>
             </tr>
             <tr>
               <td>User(s) Level</td>
               <td><?php echo $request['level']; ?></td>
             </tr>
             <tr>
               <td>Level Upgrading to</td>
               <td><?php echo $request['target_level']; ?></td>
             </tr>
             <tr>
               <td>Status</td>
               <td><?php echo $request['approval']; ?></td>
             </tr>
             <tr><td colspan="2"><button type="button" name="button" class="btn btn-primary">Next Request</button></td></tr>
                   <?php  }} ?>
               </tbody>
             </table>
      </div>
              </div>


              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- nav-tabs-custom -->
        </div>

        <div id="modalParentInfo" class="modal fade">

          </div>
