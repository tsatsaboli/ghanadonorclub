<?php
require_once '../library/config.php';
require_once '../library/functions.php';

$_SESSION['hlbank_return_url'] = $_SERVER['REQUEST_URI'];
checkUser();

$action = isset($_GET['action']) ? $_GET['action'] : '';

switch ($action) {
	
	case 'basicinfo' :
		basicinfo();
		break;
		
	case 'accountinfo' :
		accountinfo();
		break;   

    case 'changepwd' :
		changePwd();
		break;	
		
	default :
		header('Location: index.php');
}

function basicinfo()
{
	$message ='';
    $fname 	= $_POST['fullname'];
    $email 	= $_POST['email'];
    $phNumb = $_POST['phoneNo'];
    $city 	= $_POST['city'];
	$id		= $_POST['id'];

	$sql="UPDATE tbl_users u, tbl_address ad SET u.fullname = '$fname', u.email='$email', u.phone='$phNumb', ad.city='$city' WHERE u.id = ad.user_id AND u.id='$id'";
	$result = dbQuery($sql);

    header('Location: index.php?v=profile&msg=' . urlencode('Profile details has successfully being updated'));
	exit();
		
}

function accountinfo()
{
    $acctype 	= $_POST['acctype'];
    $mmName 	= $_POST['mmName'];
    $mmNumb = $_POST['mmNumb'];
	$id		= $_POST['id'];

	$sql="UPDATE tbl_accounts SET acct_type='$acctype',acct_name='$mmName',acct_number='$mmNumb' WHERE user_id='$id'";
	$result = dbQuery($sql);
    header('Location: index.php?v=profile&msg=' . urlencode('Account information has successfully being updated'));
	exit();
}

function changePwd()
{
    $pwd 	= $_POST['confirmpassword'];
	$id		= $_POST['id'];

	$sql= "UPDATE tbl_users SET pwd=PASSWORD('$pwd') WHERE id='$id'";
	$result = dbQuery($sql);

	header('Location: index.php?v=profile&msg=' . urlencode('Password details has successfully being updated'));

	exit();
}

function inviteMail()
{
    $pwd 	= $_POST['confirmpassword'];
	$id		= $_POST['id'];
header('Location: index.php?v=profile&msg=' . urlencode('Transaction Authorization Code in not valid 3.'));
	echo $pwd;
	echo $id;

	exit();
}
?>