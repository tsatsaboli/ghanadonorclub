<?php 
  $id = $_SESSION['hlbank_user']['id'];
  $result = dbQuery("SELECT * FROM tbl_users u, tbl_accounts a, tbl_address ad
  WHERE u.id = a.user_id AND ad.user_id = u.id AND u.id ='$id'");
  $row = dbFetchAssoc($result);
  $username = $row['fullname'];
  $email = $row['email'];
  $phone = $row['phone'];
  $city = $row['city'];
  $usrLevel = $row['level'];
  $acctActive=$row['status'];
  $refLink =$row['referral_code'];
  if ($acctActive !='False') {
     $msg='<p class="alert alert-success alert-dismissible"><i class="icon fa fa-check"></i>Account security update! Your account is now fully updated with your settings. Thank you</p>';
  }else{
    $msg='<p class="alert alert-error alert-dismissible"><i class="icon fa fa-check"></i>Account security update! Your account is not fully updated with your settings. Thank you</p>';
  }
  $acctype=$row['acct_type'];
  $mmName=$row['acct_name'];
  $mmNumb=$row['acct_number'];
?>
<div class="col-md-12">
<div class="box-header with-border">
          <h3 class="box-title">Profile</h3>
        </div>
          <!-- Custom Tabs -->
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#tab_1" data-toggle="tab">Basic Info</a></li>
              <li><a href="#tab_2" data-toggle="tab">Account Info</a></li>
              <li><a href="#tab_3" data-toggle="tab">Account Security</a></li>
              <li><a href="#tab_4" data-toggle="tab">Invite Users</a></li>
              <li><a href="#tab_5" data-toggle="tab">Verify Account</a></li>
            </ul>
            <div class="tab-content">
              <div class="tab-pane active" id="tab_1">
               <div class="row">
               
             <div class="box-body"><p>You can edit / update your profile information here</p>
 <form class="form-horizontal form-label-left" action="<?php echo WEB_ROOT;?>view/process.php?action=basicinfo" method="post">
              <div class="box-body">
                <div class="form-group">
                  <label for="exampleInputEmail1">Full Name</label>
                  <input type="text" name="fullname" class="form-control" id="exampleInputEmail1" placeholder="Enter email" value="<?php echo $username?>">
                  <input type="hidden" name="id" value="<?php echo $_SESSION['hlbank_user']['user_id'];?>" />
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Email</label>
                  <input type="email" name="email" class="form-control" id="exampleInputPassword1" placeholder="Password" value="<?php echo $email ?>">
                </div>
<div class="form-group">
                  <label for="exampleInputPassword1">Phone No.</label>
                  <input type="text" name="phoneNo" class="form-control" id="exampleInputPassword1" placeholder="Password" value="<?php echo $phone  ?>">
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">City</label>
                  <input type="text" name="city" class="form-control" id="exampleInputPassword1" placeholder="Password" value="<?php echo $city  ?>">
                </div>
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Save Basic Info</button>
              </div>
            </form>
             </div>

        
        <!-- ./col -->
      </div>
              </div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="tab_2">
                <div class="row">
                <div class="box-body"><p>You can edit / update your account information here</p>
              <form class="form-horizontal form-label-left" action="<?php echo WEB_ROOT;?>view/process.php?action=accountinfo" method="post">
              <div class="box-body">
                <input type="hidden" name="id" value="<?php echo $_SESSION['hlbank_user']['user_id'];?>" />
               <label> 
               <input name="acctype" type="checkbox" value="MTN" <?php echo ($acctype== 'MTN') ?  "checked" : "" ;  ?> id="acctype">MTN &nbsp
               <input name="acctype" type="checkbox" value="Vodafon" <?php echo ($acctype== 'Vodafon') ?  "checked" : "" ;  ?> id="acctype">Vodafon &nbsp
               <input name="acctype" type="checkbox" value="Tigo" <?php echo ($acctype== 'Tigo') ?  "checked" : "" ;  ?> id="acctype">Tigo &nbsp
               <input name="acctype" type="checkbox" value="Airtel" <?php echo ($acctype== 'Airtel') ?  "checked" : "" ;  ?> id="acctype">Airtel
            </label>
                <div class="form-group">
                  <label for="exampleInputEmail1">Mobile Money Name</label>
                  <input type="text" name="mmName" class="form-control" id="exampleInputEmail1" placeholder="Enter email" value="<?php echo $mmName?>">
                </div>
               <div class="form-group">
                  <label for="exampleInputEmail1">Mobile Money No.</label>
                  <input type="text" name="mmNumb" class="form-control" id="exampleInputEmail1" placeholder="Enter email" value="<?php echo $mmNumb?>">
                </div>
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Save Account Info</button>
              </div>
            </form>
             </div>
      </div>
              </div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="tab_3">
                <div class="row">
               <div class="box-body"><p>You can edit / update your account password here</p>
<form class="form-horizontal form-label-left" action="<?php echo WEB_ROOT;?>view/process.php?action=changepwd" method="post">
<input type="hidden" name="id" value="<?php echo $_SESSION['hlbank_user']['user_id'];?>" />
              <div class="box-body">
                <div class="form-group">
                  <label for="exampleInputEmail1">Current Password</label>
                  <input type="password" class="form-control" id="exampleInputEmail1" placeholder="Current Password" name="oldpassword">
                </div>
               <div class="form-group">
                  <label for="exampleInputEmail1">New Password</label>
                  <input type="password" class="form-control" id="exampleInputEmail1" placeholder="New Password" name="newpassword">
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">Confirm New Password</label>
                  <input type="password" class="form-control" id="exampleInputEmail2" placeholder="Confirm New Password" name="confirmpassword">
                  <span id='message'></span>
                </div>
                
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Save Account Security</button>
              </div>
            </form>
             </div>
      </div>
              </div>
               <div class="tab-pane" id="tab_4">
                <div class="row">
               <div class="box-body"><p>You can invite other people with your referral link here</p>
               <?php 
               if ($usrLevel !='Free user') {
                $inviteLink = 'https://www.ghanadonor.club/auth/register.php?ref='.$refLink;
                ?>
                 
                 <form class="form-horizontal form-label-left" action="" method="post">
                 <div class="box-body">
                <div class="form-group">
                  <label for="exampleInputEmail1">My invite Link</label>
                  <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Enter email" 
                  value="<?php echo $inviteLink ?>" disabled>
                </div>
               <div class="form-group">
                  <label for="exampleInputEmail1">Emails to invite</label>
                  <textarea class="form-control" name="inviteMsg"></textarea>
                  
                </div>
                
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Send Invites</button>
              </div>
            </form>

            <?php   
                }else{
                echo "<p>You are not authorise to invite people yet.</p>";
               }
               ?>
             </div>
        
        <!-- ./col -->
      </div>
              </div>
               <div class="tab-pane" id="tab_5">
                <div class="row">
               <div class="box-body"><p class="alert alert-danger alert-dismissible"><i class="icon fa fa-info"></i>Note: You should verify your account from here to keep your account security updated. Without a verified account, you will not conitnue operatin on the platform</p>
               <p><?php echo $msg?></p>
               
               </div>
        <!-- ./col -->
      </div>
              </div>
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- nav-tabs-custom -->
        </div>
        