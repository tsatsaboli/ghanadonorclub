<div class="col-md-12">
<div class="box-header with-border">
          <h3 class="box-title">Testimonials</h3>
        </div>
          <!-- Custom Tabs -->
          <div class="nav-tabs-custom">

            <?php if(isset($_GET['flash'])) {?>
              <div class="alert alert-dismissable alert-success">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <?php if($_GET['flash']){ ?>
                  <h4>Testimonial added successfully!</h4>
                  <?php }else{ ?>
                    <h4>Thank you, but it seems you have submitted a testimonial already!</h4>
                    <?php } ?>
                <!-- <p>Best check yo self, you're not looking too good. Nulla vitae elit libero, a pharetra augue. Praesent commodo cursus magna, <a href="#" class="alert-link">vel scelerisque nisl consectetur et</a>.</p> -->
              </div>
            <?php } ?>
            <ul class="nav nav-tabs">
              <li class="active"><a href="#tab_3" data-toggle="tab">Write us a testimonial</a></li>
            </ul>
            <div class="tab-content">
              <!-- /.tab-pane -->
              <div class="tab-pane active" id="tab_3">
                <div class="row">
               <div class="box-body">
               <p>Write your testimonial</p>
                 <form class="form-horizontal form-label-left" action="add_testimonial.php" method="post">
                 <div class="box-body">
               <div class="form-group">
                  <label for="exampleInputEmail1">Share your Testimonial</label>
                  <textarea class="form-control" name="message" minlength='4' spellcheck='true' required></textarea>

                </div>

                <input type="text" class='hidden' name="user_id" value="<?php echo $_SESSION['hlbank_user']['id']; ?>">
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Send your Testimonial</button>
              </div>
            </form>
             </div>
      </div>
              </div>
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- nav-tabs-custom -->
        </div>
