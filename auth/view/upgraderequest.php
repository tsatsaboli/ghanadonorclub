<div class="col-md-12">
  <div class="box-header with-border">
    <h3 class="box-title">Upgrade</h3>
  </div>
  <!-- Custom Tabs -->
  <div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
      <li class="active"><a href="#tab_2" data-toggle="tab">Pending Upgrade Request</a></li>
    </ul>
    <div class="tab-content">

      <!-- /.tab-pane -->
      <div class="tab-pane active" id="tab_2">
        <div class="row">
          <div class="box-body">
            <p>Kindly approve the following requests from users that have sent you and upgrade fee. Please make sure to verify payments before you confirm as it is irreversible after confirmation</p>
            <div class="table-responsive">
              <div class="box-body">
                <!-- /.table-responsive -->
              </div>
              <table class="table">
                <tbody>
                  <?php $requests =getUpgradeRequest($_SESSION['hlbank_user']);$i=1;  ?>

                  <?php foreach($requests as $request) {?>
                    <?php if($request['approval'] == 'pending'){ ?>
                      <tr>
                        <td><span class="fa fa-bullseye"></td>
                      </tr>
                      <tr>
                        <td>Full Name</td>
                        <td><?php echo $request['fullname']; ?></td>
                      </tr>
                      <tr>
                        <td>User(s) Level</td>
                        <td><?php echo $request['level']; ?></td>
                      </tr>
                      <tr>
                        <td>Level Upgrading to</td>
                        <td><?php echo $request['target_level']; ?></td>
                      </tr>
                      <tr >
                        <td>Payment Requied</td>
                        <td>GHC <?php echo $required_charges_by_level[$request['target_level']]; ?> <b>Approve only if payment has been made</b></td>
                      </tr>
                      <tr>
                        <td>Status</td>
                        <td><?php echo $request['approval']; ?></td>
                      </tr>
                      <tr><td colspan="2"><button  class="btn btn-sm btn-info btn-flat" onclick="approveDownline(<?php printf("%s,'%s','%s','%s'",$request['user_id'],$request['target_level'],$request['fullname'],$request['approval'])?>)"><i>Approve</i></button>

                        <button class="btn btn-sm btn-danger" onclick=""><i>Cancel</i></button></td></tr>
                        <?php  }} ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>


            <!-- /.tab-pane -->
          </div>
          <!-- /.tab-content -->
        </div>
        <!-- nav-tabs-custom -->
      </div>

      <script type="text/javascript">


        function approveDownline(id,target_level,fullname,approval){

          if(approval == 'approved'){
            alert('Action not allowed');
            return;
          }
          var confirmation = confirm(`Click ok to confirm upgrade for ${fullname}`);

          if(confirmation){

            //issue upgrade http request

            var data ={
              id: id,
              target_level: target_level,
            };

            $.post('perform_upgrade.php',data).then((response)=>{
              if(response)
              window.location = window.location;
              console.log(reponse);
            },(err)=>{
              console.log(err);
            })
          }
        }
      </script>
