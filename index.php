<?php
require_once './auth/library/config.php';
require_once './auth/library/functions.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Ghana Donor Club | Give.Take.Get Rich</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta charset="utf-8">
<meta name="keywords" content="Ghana Donor Club Give Take Get Rich" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- bootstrap-css -->
<link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<!--// bootstrap-css -->
<!-- css -->
<link rel="stylesheet" href="css/style.css" type="text/css" media="all" />
<!--// css -->
<!-- font-awesome icons -->
<link href="css/font-awesome.css" rel="stylesheet"> 
<!-- //font-awesome icons -->
<!-- font -->
<link href="//fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet">
<link href="//fonts.googleapis.com/css?family=Crimson+Text:400,400i,600,600i,700,700i" rel="stylesheet">
<link href='//fonts.googleapis.com/css?family=Roboto+Condensed:400,700italic,700,400italic,300italic,300' rel='stylesheet' type='text/css'>
<!-- //font -->
<script src="js/jquery-1.11.1.min.js"></script>
<script src="js/bootstrap.js"></script>
<script type="text/javascript">
	jQuery(document).ready(function($) {
		$(".scroll").click(function(event){		
			event.preventDefault();
			$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
		});
	});
</script> 
<!-- flexslider -->
<link rel="stylesheet" href="css/flexslider.css" type="text/css" media="screen" />
<!-- //flexslider -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<![endif]-->
</head>
<body>
	<div class="banner-top">
		<div class="slider">
			<div class="callbacks_container">
				<ul class="rslides callbacks callbacks1" id="slider4">
					<li>
						<div class="w3layouts-banner-top jarallax">
							<div class="agileinfo-dot">
								<div class="container">
									<div class="agileits-banner-info">
										<h3>50GHC sign up investment.</h3>
										<h6>Turn 50GHC into GHC320 within a day (Three hundred and twenty Ghana cedis) <br>as the initial investment by signing up on our platform.</h6>
										
										<div class="w3-button">
											<a href="auth/register.php" data-toggle="modal" style="background-color:#074b7e">Register</a><a href="auth/login.php" data-toggle="modal" style="background-color:#a4cd39">Login</a>
										</div>
									</div>	
								</div>
							</div>
						</div>
					</li>
					<li>
						<div class="w3layouts-banner-top w3layouts-banner-top2 jarallax">
							<div class="agileinfo-dot">
								<div class="container">
									<div class="agileits-banner-info">
										<h3>It all starts with you.</h3>
										<h6>It is your WHY that pushes you and edges you towards the crown of glory in the industry. <br>Discover your why and get things done quickly.</p>
										<div class="w3-button">
											<a href="auth/register.php" data-toggle="modal" style="background-color:#074b7e">Register</a><a href="auth/login.php" data-toggle="modal" style="background-color:#a4cd39">Login</a>
										</div>
									</div>	
								</div>
							</div>
						</div>
					</li>
				</ul>
			</div>
			<div class="clearfix"> </div>
			<script src="js/responsiveslides.min.js"></script>
			<script>
						// You can also use "$(window).load(function() {"
						$(function () {
						  // Slideshow 4
						  $("#slider4").responsiveSlides({
							auto: true,
							pager:true,
							nav:true,
							speed: 500,
							namespace: "callbacks",
							before: function () {
							  $('.events').append("<li>before event fired.</li>");
							},
							after: function () {
							  $('.events').append("<li>after event fired.</li>");
							}
						  });
					
						});
			</script>
			<!--banner Slider starts Here-->
		</div>
	</div>
	<!-- banner -->
	<div class="banner">
		<div class="header">
			<div class="container">
				<nav class="navbar navbar-default">
					<!-- Brand and toggle get grouped for better mobile display -->
					<div class="navbar-header">
					  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					  </button>
						<div class="w3layouts-logo">
							<h1><a href=""><img src="images/logo.png"></a></h1>
						</div>
					</div>

					<!-- Collect the nav links, forms, and other content for toggling -->
					<div class="collapse navbar-collapse nav-wil" id="bs-example-navbar-collapse-1">
						<nav>
							<ul class="nav navbar-nav">
								<li class="active"><a href="index.html">Home</a></li>
								<li><a href="#about" class="scroll">About Us</a></li>
								<li><a href="#services" class="scroll">How it Works</a></li>
								<li><a href="#markets" class="scroll">Terms of Use</a></li>
								<li><a href="#mail" class="scroll">Mail Us</a></li>
							</ul>
						</nav>
					</div>
					<!-- /.navbar-collapse -->
				</nav>
			</div>
		</div>
	</div>
	<!-- //banner -->
	<!-- modal -->
	<div class="modal about-modal fade" id="myModal" tabindex="-1" role="dialog">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header"> 
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>						
						<h4 class="modal-title">Bank <span>Online</span></h4>
					</div> 
					<div class="modal-body">
					<div class="agileits-w3layouts-info">
						<img src="images/2.jpg" alt="" />
						<p>Duis venenatis, turpis eu bibendum porttitor, sapien quam ultricies tellus, ac rhoncus risus odio eget nunc. Pellentesque ac fermentum diam. Integer eu facilisis nunc, a iaculis felis. Pellentesque pellentesque tempor enim, in dapibus turpis porttitor quis. Suspendisse ultrices hendrerit massa. Nam id metus id tellus ultrices ullamcorper.  Cras tempor massa luctus, varius lacus sit amet, blandit lorem. Duis auctor in tortor sed tristique. Proin sed finibus sem.</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- //modal -->
	<!-- about -->
	<div class="about" id="about">
		<div class="container">
			<div class="w3-about-grids">
				
				<div class="col-md-12 w3-about-left">
					<h1 style="text-align: center; color: #074b7e">Why Choose GHANA DONOR CLUB ?</h1><br><br>
					<p>In a world where no one wants to help and poverty ravaging the land. You may have wondered that with the current financial situation of your country, “how do I complete my project?? Who would help me??”</p><br>
					<p>GHANA DONOR.CLUB helps you get the money or capital you need. Whether it is a business idea or any financial need, with GHANA DONOR.CLUB you get to have an online platform that gives you access to and connects you with the people who are ready to help you.</p><br>
					<p>We at GHANA DONOR.CLUB help you with direct funding for your financial needs. With our program you are just close to financial freedom.</p><br>
					<p><strong>OUR AIM IS TO CONQUER GREED AND RAISE ENTREPRENEURS FROM AFRICA. WHEN YOU RECEIVE HUGE PAYMENTS THROUGH OUR PLATFORM, INVEST IN YOUR DREAM COMPANY AND EMPLOY MORE YOUTHS AND CONQUER THEFTS AND CORRUPTION. WE CAN ACHIEVE OUR GOLAS AND DREAMS AS A TEAM!!!</strong></p>
				</div>
				<div class="clearfix"> </div>
			</div>
		</div>
	</div>
	<!-- //about -->
		<!-- services -->
	<div class="services" id="services">
		<div class="container">
			<div class="wthree-heading">
				<h1 style="color: #074b7e">How It Works?</h1>
				<h5>Turn Ghc50 into Millions of Ghana cedis</h5>
			</div>
			<div class="agile-services-grids">
				<div class="col-md-12 agile-services-grid">
					<h6>GHC320 WITHIN A DAY</h6>
					<p>Yes!!! Make GHC320 within a day (Three hundred and twenty Ghana cedis) with only Ghc50 initial investment. Your dreams can come true!! </p><br>

<p>This is legit and there is no central account. 
Initial Start up capital is GHC50(Low risk) .
Final profit is GHC320 WITHIN A DAY.</p><br>
					<p>It's a 2×2 Member to member matrix donations. Join now it's fresh and cycle out faster than any other Multi Level Marketing (MLM) Register for free but you must pay Ghc50 within 3hrs of registration to upliner for upgrade to level 1. Note: always call upliner before and after payment for confirmation.</p><br>
<p>Level 0: This a free user level. You become an active member at level 1. At this level you pay your upliner Ghc50 for upgrade to level 1.</p><br>

<p>Level 1: At this level you invite 2 people and you also receive Ghc50 each from your 2 downlines making Ghc100Ghc Then you'll remove Ghc80 to upgrade yourself to level 2 . At this point your profit in level 1 is Ghc20</p><br>

<p>Level 2: This is the final level. At this level you receive Ghc80 from 4 people on level 2downlines and that's Ghc320. </p><br>
<p>And then u can recircle again. With only Ghc50.</p>

<p>Give glory to God and recycle back to start another level for more millions.</p>
<br>
				</div>
				<div class="clearfix"> </div>
			</div>
		</div>
	</div>
	<!-- //services -->
	<!-- markets -->
	<div class="markets" id="markets">
		<div class="container">
			<div class="wthree-heading">
				<h1 style="color: #074b7e">Terms Of Use</h1>
				<h5>TERMS AND CONDITIONS OF USE</h5>
			</div>
			<div class="markets-grids">
				<div class="col-md-12 w3ls-markets-grid">
					<div class="agileits-icon-grid">
						
						<div class="">
							<p>By signing up with GHANA DONOR.CLUB, you agree to the following Terms, Conditions and Disclaimers:</p><br>

<p>This service is provided  as an available service. We do not make warranties of any kind, either expressed or implied and we are not financial experts, so before using or making decisions from every information in GHANA DONOR.CLUB, consult a professional.</p><br>

<p>You understand and agree that there are specific guidelines and policies in place.
You also understand that you have to orient yourself of how GHANA DONOR.CLUB works before you join GHANA DONOR.CLUB, failing to UPGRADE (Upgrade, make a Ghc50 donation to the person that invited you within 3hrs) will cause the system to delete your account, you are welcome to join again and get a second 6-hours period to decide to participate, failing to participate will get you banned from joining the GHANA DONOR.CLUB community again.
You have read and understood that "Spamming" and or cross recruiting meaning contacting other participants in an attempt to promote ANY other opportunity to ANY fellow GHANA DONOR.CLUB participant is grounds for "Immediate" account suspension which disqualifies you from receiving spill over, new sign-ups as well as donations.
Under no circumstances, including negligence, shall anyone involved in creating, producing or distributing this service, be liable for any direct, indirect, incidental, special or consequential damages that result from the use of, or inability to use this service, and all the files and software contained within it, including, but not limited to, reliance on any information obtained through this service; or that result from mistakes, omissions, interruptions, deletion of files or e-mail, errors, defects, viruses, delays in operation, or transmission, or any failure of performance, whether or not limited to acts beyond our control, communications failure, theft, destruction or unauthorized access to our records, programs or services.</p><br>
<p>We reserve the right to add or remove features and modify any functionality, make changes on how the platform works, manage memberships, within GHANA DONOR.CLUB, and otherwise make changes to the service and this agreement without notice.</p><br>

<p>We may terminate users account without notice, at our sole discretion, any membership deemed to be in breach of this agreement or otherwise found to be abusing or misusing the service, or harassing the other members or administrator in any way.</p><br>

<p>In the unlikely event that this program should ever terminate its operations, it's creator, operators, employees, assigns and successors shall not be held liable for any loss whatsoever to our members.</p><br>

<p>Sites and individuals involved with the following activities are NOT ELIGIBLE: selling, providing or linking to unlicensed content, pornography, warez, pirated software, hacking or spamming software, email address lists or harvesting software, or any materials endorsing violence, hatred, revenge, racism, victimization, or criminal activity.</p><br>

<p>We make no claims on how much money you can make with our program. Your ability to earn depends on a number of factors, including where and how (and how often) you advertise the program, and the motivation and ability of those in your up lines, to make referrals. Individual results will vary.</p><br>

<p>Members caught spamming or otherwise causing harm to our program will have their accounts terminated, and maybe prosecuted for their actions. We will investigate all allegations before taking action.</p><br>

<p>All donations you willingly and directly send to a fellow participant are final. No refunds.</p><br>

<p>You understand that all donations will be made directly by donors to your Mobile money or bank accounts, and that any problems you have concerning such matters should be taken up with your respective merchant or account provider.</p><br>

<p>You agree to accept email updates regarding GHANA DONOR.CLUB. We will never bombard you with emails or spam. Report spam and other abuse via the contact form.</p><br>

<p>Please include the entire spam email with headers.</p><br>

<p>If any member is caught stacking, GHANA DONOR.CLUB reserves the right to move the stacked accounts further down the matrix and/or block the stacked accounts.</p><br>

<p><strong>Enjoy giving and receiving donations with <br>
GHANA DONOR.CLUB<br>
TEAM, GHANA DONOR.CLUB</strong></p>

						</div>
						<div class="clearfix"> </div>
					</div>
				</div>
				<div class="clearfix"> </div>
			</div>
		</div>
	</div>
	<!-- //markets -->

	<!-- testimonial -->
	<div class="jarallax testimonial" id="testimonial">
		<div class="testimonial-dot">
			<div class="container">
				<div class="wthree-heading testimonial-heading">
					<h3>Testimonials</h3>
					<h5>What people are saying about us</h5>
				</div>
				<div class="w3-agile-testimonial">
					<div class="slider">
						<div class="callbacks_container">
						<ul class="rslides callbacks callbacks1" id="slider3">
						<?php 
						  $result = dbQuery("SELECT * FROM tbl_users u, testimonials t 
						  WHERE u.id = t.user_id AND t.approval='1'");
						  while ($row = dbFetchAssoc($result)) 
						  	{
						    $name = $row["fullname"];
						    $msg = $row['message'];

						    ?>   
								<li>
									<div class="testimonial-img-info">
										<p><?php echo $msg?></p>
										<h5><?php echo $name  ?></h5>	
									</div>
								</li>
							
						    <?php 
                              }
						?>
						</ul>
						</div>
						<div class="clearfix"> </div>
						<script>
									// You can also use "$(window).load(function() {"
									$(function () {
									  // Slideshow 4
									  $("#slider3").responsiveSlides({
										auto: true,
										pager:false,
										nav:false,
										speed: 500,
										namespace: "callbacks",
										before: function () {
										  $('.events').append("<li>before event fired.</li>");
										},
										after: function () {
										  $('.events').append("<li>after event fired.</li>");
										}
									  });
								
									});
						</script>
						<!--banner Slider starts Here-->
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- //testimonial -->
	<!-- blog -->

	<!-- //blog -->
	<!-- contact -->
	<div class="contact" id="mail">
		<div class="container">
			<div class="wthree-heading">
				<h3>Get In Touch</h3>
				<h5>Need help? Please complete the form below or call us.</h5>
			</div>
			<div class="agile-contact-form">
				<div class="col-md-6 contact-form-left">
					<div class="w3layouts-contact-form-top">
						<h3>Contact Details</h3>
						<p>We will help you with all your needs. Our team will answer all your question and assist you with all your questions and directions.</p>
					</div>
					<div class="agileits-contact-address">
						<ul>
							<li><i class="fa fa-phone" aria-hidden="true"></i> <span>+233558749927</span></li>
							<li><i class="fa fa-phone fa-envelope" aria-hidden="true"></i> <span><a href="mailto:ghanadonorclub@gmail.com">ghanadonorclub@gmail.com</a></span></li>
							<li><i class="fa fa-map-marker" aria-hidden="true"></i> <span>Accra Ghana</span></li>
						</ul>
					</div>
				</div>
				<div class="col-md-6 contact-form-right">
					<div class="contact-form-top">
						<h3>Send us a message</h3>
					</div>
					<div class="agileinfo-contact-form-grid">
						   <form  action="" method="POST" enctype="multipart/form-data">
                            <input type="hidden" name="action" value="submit">
							<input type="text" name="Name" placeholder="Name" required="">
							<input type="email" name="Email" placeholder="Email" required="">
							<input type="text" name="Telephone" placeholder="Telephone" required="">
							<textarea name="Message" placeholder="Message" required=""></textarea>
							<button class="btn1" type="submit" name="contactSubmitBtn">Submit</button>
						</form>
						<?php
if(isset($_POST['contactSubmitBtn'])) {
 
    // EDIT THE 2 LINES BELOW AS REQUIRED
    $email_to = "no-reply@ghanadonor.club";
    $email_subject = "New Message From: ".$_POST['Name'];
 
    function died($error) {
        // your error code can go here
        echo "We are very sorry, but there were error(s) found with the form you submitted. ";
        echo "These errors appear below.<br /><br />";
        echo $error."<br /><br />";
        echo "Please go back and fix these errors.<br /><br />";
        die();
    }
 
 
    // validation expected data exists
    if(!isset($_POST['Name']) ||
        !isset($_POST['Email']) ||
        !isset($_POST['Telephone']) ||
        !isset($_POST['Message'])) {
        died('We are sorry, but there appears to be a problem with the form you submitted.');       
    }
 
     
 
    $first_name = $_POST['Name']; // required
    $email_from = $_POST['Email']; // required
    $telephone = $_POST['Telephone']; // not required
    $comments = $_POST['Message']; // required
 
    $error_message = "";
    $email_exp = '/^[A-Za-z0-9._%-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/';
 
  if(!preg_match($email_exp,$email_from)) {
    $error_message .= 'The Email Address you entered does not appear to be valid.<br />';
  }
 
    $string_exp = "/^[A-Za-z .'-]+$/";
 
  if(!preg_match($string_exp,$first_name)) {
    $error_message .= 'The First Name you entered does not appear to be valid.<br />';
  }
 
  if(strlen($comments) < 2) {
    $error_message .= 'The Comments you entered do not appear to be valid.<br />';
  }
 
  if(strlen($error_message) > 0) {
    died($error_message);
  }
 
    //$email_message = "Form details below.\n\n";
 
     
    function clean_string($string) {
      $bad = array("content-type","bcc:","to:","cc:","href");
      return str_replace($bad,"",$string);
    }
 
    $email_message .= "Message: ".clean_string($comments)."\n";
    //$email_message .= "\n";
    $email_message .= "Telephone: ".clean_string($telephone)."\n";
 
// create email headers
$headers = 'From: '.$email_from."\r\n".
'Reply-To: '.$email_from."\r\n" .
'X-Mailer: PHP/' . phpversion();
@mail($email_to, $email_subject, $email_message, $headers);  
?>
 
<!-- include your own success html here -->
 
Thank you for contacting us. We will be in touch with you very soon.
 
<?php
 
}
?>
					</div>
				</div>
				<div class="clearfix"> </div>
			</div>
		</div>
	</div>
	<!-- //contact -->
	<!-- copyright -->
	<footer>
		<div class="container">
			<div class="copyright">
				<p>© 2017 Ghana Donor Club. All rights reserved</p>
			</div>
		</div>
	</footer>
	<!-- //copyright -->
	<script src="js/jarallax.js"></script>
	<script src="js/SmoothScroll.min.js"></script>
	<script type="text/javascript">
		/* init Jarallax */
		$('.jarallax').jarallax({
			speed: 0.5,
			imgWidth: 1366,
			imgHeight: 768
		})
	</script>
	<script src="js/responsiveslides.min.js"></script>
	<script type="text/javascript" src="js/move-top.js"></script>
	<script type="text/javascript" src="js/easing.js"></script>
	<!-- here stars scrolling icon -->
	<script type="text/javascript">
		$(document).ready(function() {
			/*
				var defaults = {
				containerID: 'toTop', // fading element id
				containerHoverID: 'toTopHover', // fading element hover id
				scrollSpeed: 1200,
				easingType: 'linear' 
				};
			*/
								
			$().UItoTop({ easingType: 'easeOutQuart' });
								
			});
	</script>
<!-- //here ends scrolling icon -->
<!-- FlexSlider -->
  <script defer src="js/jquery.flexslider.js"></script>
	<script type="text/javascript">
    $(window).load(function(){
      $('#carousel').flexslider({
        animation: "slide",
        controlNav: false,
        animationLoop: true,
        slideshow: false,
        itemWidth: 102,
        itemMargin: 5,
        asNavFor: '#slider'
      });

      $('#slider').flexslider({
        animation: "slide",
        controlNav: false,
        animationLoop: true,
        slideshow: true,
        sync: "#carousel",
        start: function(slider){
          $('body').removeClass('loading');
        }
      });
    });
  </script>
<!-- //FlexSlider -->
</body>	
</html>