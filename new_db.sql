-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 21, 2017 at 01:55 AM
-- Server version: 10.1.9-MariaDB
-- PHP Version: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `trth`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_accounts`
--

CREATE TABLE `tbl_accounts` (
  `id` int(10) NOT NULL,
  `user_id` int(10) NOT NULL,
  `level` varchar(100) NOT NULL,
  `type` varchar(150) NOT NULL,
  `acct_type` varchar(150) NOT NULL,
  `acct_name` varchar(100) NOT NULL,
  `acct_number` varchar(250) NOT NULL,
  `pin` varchar(150) NOT NULL,
  `status` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_accounts`
--

INSERT INTO `tbl_accounts` (`id`, `user_id`, `level`, `type`, `acct_type`, `acct_name`, `acct_number`, `pin`, `status`) VALUES
(1, 1, 'Level 3', 'MobileMoney', 'MTN', 'admin1', '0558749927', 'admin1', 'True'),
(2, 2, 'Level 2', 'MobileMoney', 'MTN', 'admin2', '0558749927', 'admin2', 'True'),
(3, 3, 'Level 1', 'MobileMoney', 'Airtel', 'admin3', '0558749927', 'admin3', 'True'),
(19, 19, 'Level 2', 'MobileMoney', 'MTN', 'Cephas mmadu', '0272552255', 'Cephas', 'True'),
(20, 20, 'Level 2', 'MobileMoney', 'MTN', 'Hayford Kofi Offei', '0542729410', 'hayfy', 'True'),
(21, 21, 'Level 2', 'MobileMoney', 'MTN', 'Lydia Sakyiwaa Appiah', '0244974689', 'Lysah', 'True'),
(22, 22, 'Level 1', 'MobileMoney', 'MTN', 'Abdul mubarak ibrahim', '0248200314', 'deals1', 'True'),
(23, 23, 'Level 1', 'MobileMoney', 'MTN', 'cephas mmadu', '0558749927', 'Cephas1', 'True'),
(24, 24, 'Level 1', 'MobileMoney', 'MTN', 'ransford hughes', '0548287971', 'ransford', 'True'),
(29, 29, 'Free user', 'MobileMoney', 'MTN', 'Nartey Joseph', 'No', 'JOSEPH', 'False'),
(30, 30, 'Free user', 'MobileMoney', 'MTN', 'grace', '0243499400', 'dalynton', 'False'),
(31, 31, 'Free user', 'MobileMoney', 'MTN', 'Emmanuel Obeng', '0541771936', 'Bra Qweku', 'False'),
(32, 32, 'Free user', 'MobileMoney', 'MTN', 'Gyamfi Konadu', '0249010459', 'Caleb1', 'False'),
(33, 33, 'Free user', 'MobileMoney', 'Vodafon', 'kweame', '0244775589', 'admin000', 'False'),
(34, 34, 'Free user', 'MobileMoney', 'Vodafon', 'Osei Richard', '0244775589', 'admin401', 'True'),
(35, 35, 'Free user', 'MobileMoney', 'Vodafon', 'kweame222', '0244775589', 'kofikofi', 'True'),
(36, 36, 'Level 2', 'MobileMoney', 'Vodafon', 'Osei Richard', '0244775589', 'admin33', 'completed'),
(37, 37, 'Free user', 'MobileMoney', 'Airtel', 'Osei Bollege', '0200902221', 'adminq', 'True'),
(38, 38, 'Free user', 'MobileMoney', 'Vodafon', 'kweame', '0244775589', 'memememe', 'False'),
(39, 39, 'Free user', 'MobileMoney', 'Vodafon', '', '', 'ad1234', 'False'),
(40, 40, 'Free user', 'MobileMoney', 'Vodafon', 'Osei Richard', '0244775589', 'admin212', 'False'),
(43, 43, 'Free user', 'MobileMoney', 'Vodafon', 'Osei Richard', '0244775589', 'admin', 'False'),
(44, 44, 'Level 1', 'MobileMoney', 'Vodafon', 'kweame', '0244775589', 'admin1234', 'True'),
(45, 45, 'Level 1', 'MobileMoney', 'Vodafon', 'Osei Richard', '0244775589', 'adminaccra', 'True'),
(46, 46, 'Free user', 'MobileMoney', 'Tigo', 'Osei Richard', '0244775589', 'adminacccra', 'False'),
(47, 47, 'Free user', 'MobileMoney', 'Tigo', 'kweame', '0244775589', 'admin676', 'False'),
(48, 48, 'Free user', 'MobileMoney', 'Vodafon', 'Osei Bollege', '0200902221', 'all4real', 'False'),
(49, 49, 'Free user', 'MobileMoney', 'Tigo', 'Osei Richard', '0244775589', 'admin2020', 'False'),
(50, 50, 'Level 1', 'MobileMoney', 'Airtel', 'kweame213', '0244775589', 'admin12345', 'True'),
(51, 51, 'Free user', 'MobileMoney', 'Vodafon', 'kweame', '0244775589', 'admin', 'False'),
(52, 52, 'Free user', 'MobileMoney', 'Vodafon', 'kweame', '0244775589', 'admin444444444', 'False'),
(53, 53, 'Free user', 'MobileMoney', 'Airtel', 'kweame', '0244775589', 'admin', 'False'),
(54, 54, 'Free user', 'MobileMoney', 'Vodafon', 'kweame', '0244775589', 'admin', 'False'),
(55, 55, 'Free user', 'MobileMoney', 'Vodafon', 'kweame', '0244775589', 'adminnewone', 'False');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_activity_log`
--

CREATE TABLE `tbl_activity_log` (
  `id` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `date` varchar(100) NOT NULL,
  `action` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_address`
--

CREATE TABLE `tbl_address` (
  `id` int(10) NOT NULL,
  `user_id` int(10) NOT NULL,
  `city` varchar(50) DEFAULT NULL,
  `country` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_address`
--

INSERT INTO `tbl_address` (`id`, `user_id`, `city`, `country`) VALUES
(1, 1, 'Accra', 'Ghana'),
(2, 2, 'Accra', 'Ghana'),
(3, 3, 'Accra', 'Ghana'),
(19, 19, 'Accra', 'Ghana'),
(20, 20, 'Accra', 'Ghana'),
(21, 21, 'Kumasi', 'Ghana'),
(22, 22, 'Nkawkaw', 'Ghana'),
(23, 23, 'Accra', 'Ghana'),
(24, 24, 'Accra', 'Ghana'),
(29, 29, 'Accra', 'Ghana'),
(30, 30, 'Accra', 'Ghana'),
(31, 31, 'Obuasi', 'Ghana'),
(32, 32, 'Accra', 'Ghana'),
(33, 33, 'Accra', 'Ghana'),
(34, 34, 'kumasi', 'Ghana'),
(35, 35, 'Accra', 'Ghana'),
(36, 36, 'kumasi', 'Ghana'),
(37, 37, 'Accra', 'Ghana'),
(38, 38, 'Accra', 'Ghana'),
(39, 39, 'Accra', 'Ghana'),
(40, 40, 'kumasi', 'Ghana'),
(41, 41, 'Accra', 'Ghana'),
(42, 42, 'Accra', 'Ghana'),
(43, 43, 'Accra', 'Ghana'),
(44, 44, 'Accra', 'Ghana'),
(45, 45, 'Accra', 'Ghana'),
(46, 46, 'Accra', 'Ghana'),
(47, 47, 'Accra', 'Ghana'),
(48, 48, 'Accra', 'Ghana'),
(49, 49, 'kumasi', 'Ghana'),
(50, 50, 'Accra', 'Ghana'),
(51, 51, 'Accra', 'Ghana'),
(52, 52, 'Accra', 'Ghana'),
(53, 53, 'Accra', 'Ghana'),
(54, 54, 'Accra', 'Ghana'),
(55, 55, 'Accra', 'Ghana');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE `tbl_users` (
  `id` int(10) NOT NULL,
  `fullname` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL,
  `pwd` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `gender` varchar(10) NOT NULL,
  `is_active` varchar(10) NOT NULL,
  `utype` varchar(10) NOT NULL,
  `usrip` varchar(100) NOT NULL,
  `inputteddate` varchar(100) NOT NULL,
  `referral_by` varchar(250) NOT NULL,
  `referral_code` varchar(250) DEFAULT NULL,
  `original_referral_by` varchar(128) DEFAULT NULL,
  `compel_upgrade_time` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`id`, `fullname`, `username`, `pwd`, `email`, `phone`, `gender`, `is_active`, `utype`, `usrip`, `inputteddate`, `referral_by`, `referral_code`, `original_referral_by`, `compel_upgrade_time`) VALUES
(1, 'Administrator1', '8822832317', '*A4B6157319038724E3560894F7F932C8886EBFCF', 'admin1@admin.com', '0558749927', 'Male', 'True', 'User', '41.66.195.164', '2017-06-23 11:46:26', '', '7I6TK93G', '', NULL),
(2, 'Administrator2', '5238085981', '*A4B6157319038724E3560894F7F932C8886EBFCF', 'admin2@admin.com', '0558749927', 'Male', 'True', 'User', '41.66.195.164', '2017-06-23 11:49:28', '7I6TK93G', '19VK5G6Q', '7I6TK93G', NULL),
(3, 'Administrator3', '1677438653', '*A4B6157319038724E3560894F7F932C8886EBFCF', 'admin3@admin.com', '0558749927', 'Male', 'True', 'User', '41.66.195.164', '2017-06-23 11:55:50', '19VK5G6Q', '5CO2F8R7', '19VK5G6Q', NULL),
(19, 'Cephas Mmadu', '5187807500', '*A4B6157319038724E3560894F7F932C8886EBFCF', 'cephasmmadu@gmail.com', '0272552255', 'Male', 'True', 'User', '41.66.255.29', '2017-06-25 07:20:14', '5CO2F8R7', '0A09TP1S', '5CO2F8R7', NULL),
(20, 'Hayford Kofi Offei', '4256741479', '*A4B6157319038724E3560894F7F932C8886EBFCF', 'kofioffei@gmail.com', '0542729410', 'Male', 'True', 'User', '197.251.240.162', '2017-06-25 07:47:41', '0A09TP1S', '4CYI413J', '0A09TP1S', NULL),
(21, 'Lidea Apa', '5368901988', '*A4B6157319038724E3560894F7F932C8886EBFCF', 'appiahn@yahoo.com', 'O244974689', 'Female', 'True', 'User', '41.189.160.237', '2017-06-25 07:55:25', '0A09TP1S', 'N09CA7I0', '0A09TP1S', NULL),
(22, 'Mubarak ibrahim', '7229849150', '*A4B6157319038724E3560894F7F932C8886EBFCF', 'imubarak942@gmail.com', '0248200314', 'Male', 'True', 'User', '41.189.160.210', '2017-06-25 08:30:30', '4CYI413J', '121RB4PU', '4CYI413J', NULL),
(23, 'Cephas phones', '9429967015', '*A4B6157319038724E3560894F7F932C8886EBFCF', 'mmaducephas@gmail.com', '0272552255', 'Male', 'True', 'User', '41.66.255.29', '2017-06-25 09:39:32', 'N09CA7I0', 'YB6203ZP', 'N09CA7I0', NULL),
(24, 'Ransford', '7782345370', '*A4B6157319038724E3560894F7F932C8886EBFCF', 'rannyted12345@outlook.com', '0542729410', 'Male', 'True', 'User', '41.189.160.47', '2017-06-25 09:57:28', 'N09CA7I0', '50D3OJF3', 'N09CA7I0', NULL),
(29, 'Nartey Joseph', '5338759776', '*A4B6157319038724E3560894F7F932C8886EBFCF', 'josephappenteng1@gmail.com', '0543425701', 'Male', 'True', 'User', '41.189.160.220', '2017-06-27 08:05:20', '121RB4PU', 'K3TX21J2', '121RB4PU', NULL),
(30, 'Quasi', '8534258897', '*A4B6157319038724E3560894F7F932C8886EBFCF', 'owusu.daniel62@yahoo.com', '0243499400', 'Male', 'True', 'User', '41.189.160.60', '2017-06-27 08:14:06', 'O8Y4T17Y', '6H0P4KW0', 'O8Y4T17Y', NULL),
(31, 'OBENG DWOMOH', '2398782491', '*A4B6157319038724E3560894F7F932C8886EBFCF', 'obengdwomoh2014@gmail.com', '0541771936', 'Male', 'True', 'User', '41.189.160.198', '2017-06-27 08:14:07', '50D3OJF3', '64QQZ73C', '50D3OJF3', NULL),
(32, 'Gyamfi Konadu Caleb', '1414337011', '*A4B6157319038724E3560894F7F932C8886EBFCF', 'candour75@gmail.com', '0249010459', 'Male', 'True', 'User', '41.189.161.39', '2017-06-27 08:38:40', '121RB4PU', 'VV8795QX', '121RB4PU', NULL),
(33, 'kojo4', '1329625382', '*A4B6157319038724E3560894F7F932C8886EBFCF', 'iglochard@outlook.com', '0200902221', 'Male', 'True', 'User', '::1', '2017-06-27 10:40:55', '5CO2F8R7', 'AV7R280H', '7I6TK93G', NULL),
(34, 'kojo abrefi2', '1323161716', '*A4B6157319038724E3560894F7F932C8886EBFCF', 'iglochard@outlook.com', '0200902221', 'Male', 'True', 'User', '::1', '2017-06-27 11:36:55', 'K8C80S0Z', '841YNUI8', '7I6TK93G', NULL),
(35, 'Osei Richard 32017', '1264680364', '*A4B6157319038724E3560894F7F932C8886EBFCF', 'iglochard@outlook.com', '0200902221', 'Male', 'True', 'User', '::1', '2017-06-27 12:18:32', 'K8C80S0Z', 'CK5624ZT', '7I6TK93G', NULL),
(36, 'kojo2221', '1351222328', '*A4B6157319038724E3560894F7F932C8886EBFCF', 'iglochard@outlook.com', '0200902221', 'Male', '0', 'User', '::1', '2017-06-27 12:20:34', 'A96A7E4P', '173VNF3P', '7I6TK93G', '2017-07-21 00:43:24'),
(37, 'dsd2017', '1382123395', '*A4B6157319038724E3560894F7F932C8886EBFCF', 'iglochard@outlook.com', '54545', 'Female', 'True', 'User', '::1', '2017-06-27 12:21:58', 'A96A7E4P', '74K5C9LW', '7I6TK93G', NULL),
(38, 'AMAAAAAAAAAAAAA', '1395851272', '*A4B6157319038724E3560894F7F932C8886EBFCF', 'iglochard@outlook.com', '0200902221', 'Female', 'True', 'User', '::1', '2017-06-27 13:22:59', '841YNUI8', 'DTA6940T', '7I6TK93G', NULL),
(39, 'df ananana', '1263132643', '*A4B6157319038724E3560894F7F932C8886EBFCF', 'igl3@outlook.com', '0200902221', 'Male', 'True', 'User', '::1', '2017-06-27 13:25:45', '841YNUI8', '8WX9PM45', '7I6TK93G', NULL),
(40, 'Osei Richard 33', '1311046790', '*A4B6157319038724E3560894F7F932C8886EBFCF', 'igl3ochard@outlook.com', '0200902221', 'Male', 'True', 'User', '::1', '2017-06-27 13:27:39', 'CK5624ZT', 'L5G4M8T9', '7I6TK93G', NULL),
(41, 'Osei Richard 3', '1228543135', '*A4B6157319038724E3560894F7F932C8886EBFCF', 'sdcsd@gma.com', '0200902221', 'Female', 'True', 'User', '::1', '2017-06-27 13:40:06', 'CK5624ZT', 'UD58HY77', '7I6TK93G', NULL),
(42, 'Osei Richard 3', '1391581695', '*A4B6157319038724E3560894F7F932C8886EBFCF', 'iglochard@outlook.com', '0200902221', 'Male', 'True', 'User', '::1', '2017-06-27 13:50:51', 'AV7R280H', 'Q4MH12E4', '7I6TK93G', NULL),
(43, 'ooohhhhhhhhhh', '1281883204', '*A4B6157319038724E3560894F7F932C8886EBFCF', 'iglochard@outlook.com', '0200902221', 'Female', 'True', 'User', '::1', '2017-06-27 14:17:54', 'AV7R280H', 'FE5664ON', '7I6TK93G', NULL),
(44, 'Osei Richard 3', '1399225425', '*A4B6157319038724E3560894F7F932C8886EBFCF', 'iglochard@outlook.com', '0200902221', 'Male', 'True', 'User', '::1', '2017-06-27 14:19:07', '173VNF3P', '1I385XKH', '7I6TK93G', NULL),
(45, 'amamkofi', '1257487312', '*A4B6157319038724E3560894F7F932C8886EBFCF', 'iglochard@outlook.com', '0200902221', 'Male', 'True', 'User', '::1', '2017-06-27 16:36:31', '173VNF3P', 'A12JOE09', '7I6TK93G', '2017-07-13 02:28:09'),
(46, 'Osei Richard 3345', '1334861544', '*A4B6157319038724E3560894F7F932C8886EBFCF', 'iglochard@outlook.com', '0200902221', 'Male', 'True', 'User', '::1', '2017-06-27 16:38:07', '74K5C9LW', '831SVRB4', '7I6TK93G', NULL),
(47, 'kojo abrefi 3', '1271007641', '*A4B6157319038724E3560894F7F932C8886EBFCF', 'sdcsd@gma.com', '0200902221', 'Male', 'True', 'User', '::1', '2017-06-27 17:09:33', '74K5C9LW', '52EAP4F0', '7I6TK93G', NULL),
(48, 'Osei Richard 333', '1291549053', '*A4B6157319038724E3560894F7F932C8886EBFCF', 'iglochard@outlook.com', '0200902221', 'Male', 'True', 'User', '::1', '2017-06-28 03:17:40', 'DTA6940T', 'D1W4V35L', '7I6TK93G', NULL),
(49, 'Osei Richard 3333', '1347278899', '*A4B6157319038724E3560894F7F932C8886EBFCF', 'iglochard@outlook.com', '0200902221', 'Male', 'True', 'User', '::1', '2017-06-28 03:30:03', 'D1W4V35L', 'ZUR0652X', 'D1W4V35L', NULL),
(50, 'Osei Richard 3455555', '1375552990', '*A4B6157319038724E3560894F7F932C8886EBFCF', 'iglochard@outlook.com', '0200902221', 'Male', 'True', 'User', '::1', '2017-06-28 03:41:50', 'ZUR0652X', 'A75H0UP2', 'ZUR0652X', NULL),
(51, 'Osei Richard 3', '1323019397', '*A4B6157319038724E3560894F7F932C8886EBFCF', 'iglochard@outlook.com', '0200902221', 'Male', 'True', 'User', '::1', '2017-06-28 03:49:11', 'Q4MH12E4', '449E8FWE', 'Q4MH12E4', NULL),
(52, 'Osei Richard 3444444', '1216339259', '*A4B6157319038724E3560894F7F932C8886EBFCF', 'iglochard@outlook.com', '0200902221', 'Male', 'True', 'User', '::1', '2017-06-28 03:51:36', 'DTA6940T', 'YS5JT301', '7I6TK93G', NULL),
(53, 'Osei Richard 3777', '1243445146', '*A4B6157319038724E3560894F7F932C8886EBFCF', 'iglochard@outlook.com', '0200902221', 'Female', 'True', 'User', '::1', '2017-06-28 03:52:49', '8WX9PM45', '6BHL588Q', '7I6TK93G', NULL),
(54, 'dsdffff', '1240474232', '*A4B6157319038724E3560894F7F932C8886EBFCF', 'iglochard@outlook.com', '0200902221', 'Female', 'True', 'User', '::1', '2017-07-03 22:16:34', 'Q4MH12E4', '2QBA938Z', 'Q4MH12E4', NULL),
(55, 'i am new', '1338549985', '*A4B6157319038724E3560894F7F932C8886EBFCF', 'iglochard@outlook.com', '200902221', 'Male', 'True', 'User', '::1', '2017-07-04 02:15:38', '8WX9PM45', '853BON7G', '7I6TK93G', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user_log`
--

CREATE TABLE `tbl_user_log` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `username` varchar(25) NOT NULL,
  `login_date` varchar(30) NOT NULL,
  `logout_date` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `testimonials`
--

CREATE TABLE `testimonials` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `message` text NOT NULL,
  `approval` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `testimonials`
--

INSERT INTO `testimonials` (`id`, `user_id`, `message`, `approval`) VALUES
(15, 36, 'oh wow', 0);

-- --------------------------------------------------------

--
-- Table structure for table `upgrade_requests`
--

CREATE TABLE `upgrade_requests` (
  `user_id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `target_level` text NOT NULL,
  `approval` text NOT NULL,
  `id` int(11) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `upgrade_requests`
--

INSERT INTO `upgrade_requests` (`user_id`, `parent_id`, `target_level`, `approval`, `id`, `updated_at`) VALUES
(12, 3, 'Level 1', 'pending', 11, '2017-07-13 02:33:28'),
(19, 3, 'Level 1', 'approved', 12, '2017-07-13 02:33:28'),
(20, 19, 'Level 1', 'approved', 13, '2017-07-13 02:33:28'),
(22, 20, 'Level 1', 'approved', 14, '2017-07-13 02:33:28'),
(21, 19, 'Level 1', 'approved', 15, '2017-07-13 02:33:28'),
(23, 21, 'Level 1', 'approved', 16, '2017-07-13 02:33:28'),
(25, 20, 'Level 1', 'approved', 17, '2017-07-13 02:33:28'),
(19, 2, 'Level 2', 'approved', 18, '2017-07-13 02:33:28'),
(20, 3, 'Level 2', 'approved', 19, '2017-07-13 02:33:28'),
(24, 21, 'Level 1', 'approved', 20, '2017-07-13 02:33:28'),
(21, 3, 'Level 2', 'approved', 21, '2017-07-13 02:33:28'),
(31, 24, 'Level 1', 'pending', 22, '2017-07-13 02:33:28'),
(50, 49, 'Level 1', 'approved', 23, '2017-07-13 02:33:28'),
(49, 48, 'Level 1', 'pending', 26, '2017-07-13 02:33:28'),
(40, 35, 'Level 1', 'pending', 34, '2017-07-13 02:33:28'),
(55, 39, 'Level 1', 'pending', 35, '2017-07-13 02:33:28'),
(44, 36, 'Level 2', 'approved', 36, '2017-07-13 02:33:28'),
(27, 1, 'Level 1', 'pending', 37, '2017-07-13 02:33:28'),
(45, 36, 'Level 2', 'approved', 38, '2017-07-04 02:33:28'),
(36, 28, 'Level 1', 'approved', 39, '2017-07-13 02:33:28'),
(34, 27, 'Level 1', 'pending', 40, '2017-07-13 11:31:55'),
(35, 27, 'Level 1', 'pending', 41, '2017-07-16 21:34:18');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_accounts`
--
ALTER TABLE `tbl_accounts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_activity_log`
--
ALTER TABLE `tbl_activity_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_address`
--
ALTER TABLE `tbl_address`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_user_log`
--
ALTER TABLE `tbl_user_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `testimonials`
--
ALTER TABLE `testimonials`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `upgrade_requests`
--
ALTER TABLE `upgrade_requests`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_accounts`
--
ALTER TABLE `tbl_accounts`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;
--
-- AUTO_INCREMENT for table `tbl_activity_log`
--
ALTER TABLE `tbl_activity_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_address`
--
ALTER TABLE `tbl_address`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;
--
-- AUTO_INCREMENT for table `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;
--
-- AUTO_INCREMENT for table `tbl_user_log`
--
ALTER TABLE `tbl_user_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `testimonials`
--
ALTER TABLE `testimonials`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `upgrade_requests`
--
ALTER TABLE `upgrade_requests`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
